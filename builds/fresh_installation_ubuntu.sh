sudo apt-get update
sudo apt-get upgrade -y

# common
sudo apt-get install -y git

# python
sudo apt-get install -y python3.11 python3.11-venv python3.11-pip python3.11-dev

# kivy
# Install necessary system packages
sudo apt-get install -y \
    libsdl2-dev \
    libsdl2-image-dev \
    libsdl2-mixer-dev \
    libsdl2-ttf-dev \
    libportmidi-dev \
    libswscale-dev \
    libavformat-dev \
    libavcodec-dev \
    zlib1g-dev

# Install gstreamer for audio, video (optional)
sudo apt-get install -y \
    libgstreamer1.0 \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good

python3.11 -m venv .venv
source .venv/bin/activate

pip install --upgrade pip
python -m pip install kivy --pre --no-deps --index-url  https://kivy.org/downloads/simple/
python -m pip install "kivy[base]" --pre --extra-index-url https://kivy.org/downloads/simple/

pip install -r requirements.txt