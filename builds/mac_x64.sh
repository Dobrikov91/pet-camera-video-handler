#pyinstaller -y --clean --windowed --name PetCam \
#  --exclude-module _tkinter \
#  --exclude-module Tkinter \
#  --exclude-module enchant \
#  --exclude-module twisted \
#  ./main.py
# bumpversion --config-file .bumpversion.cfg patch

arch -x86_64 ../.venv/bin/python ../.venv/bin/pyinstaller -y --clean ./PetCamMac.spec

pushd dist
hdiutil create ./PetCam.dmg -srcfolder PetCam.app -ov
popd
