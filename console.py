import sys

from detectors.background_subtractor import BackgroundSubtractor
from detectors.contours import Contours, ContoursTypes
from effects.motion_detection_base_frame import MotionDetectionBaseFrame
from effects.trailing import Trailing
from input.recorder import Recorder
from input.stream import Stream
from model import Model

if __name__ == '__main__':
    url = 0
    if len(sys.argv) > 1:
        url = sys.argv[1]

    model = Model(
        Stream(
            url=url,
            motionDetectionBaseFrame=MotionDetectionBaseFrame(
                scaleFactor=1,
                kernel=25),
            imageEffect=None),
        BackgroundSubtractor(30, 3, 0.01),
        None,
        Contours(0, ContoursTypes.Precise.name, 2),
        motionWindow=0)

    if len(sys.argv) > 2:
        model.recorder = Recorder(
            path='./Console.mp4',
            resolution=model.frameProvider.resolution,
            fps=30,
            recordingSpeed=2,
            splitByDays=1)

    # preview = Preview('Model')

    model.frameProvider.start()

    try:
        while True:
            model.processFrame()
            if model.resultFrame is not None and model.frameProvider:
                if model.recorder:
                    model.recorder.addFrame(model.resultFrame)

                print(int(model.fpsCounter.averageFps))

    except KeyboardInterrupt:
        pass

    if model.frameProvider:
        model.frameProvider.stop()
