import random
from datetime import datetime, timedelta
from typing import Union

from kivy.app import App
from kivy.clock import Clock
from kivy.config import ConfigParser

from detectors.background_subtractor import BackgroundSubtractor
from detectors.contours import Contours
from detectors.custom_detector import CustomDetector
from detectors.motion_detector import MotionDetector, MotionDetectorTypes
from detectors.night_vision_detector import NightVisionDetector
from effects.blur import Blur
from effects.image_effect import ImageEffect, ImageEffectTypes
from effects.motion_detection_base_frame import MotionDetectionBaseFrame
from effects.trailing import Trailing, TrailingType
from input.file import File
from input.file_list import FileList
from input.recorder import Recorder
from input.stream import Stream
from items.about_dialog import AboutDialog
from items.update_dialog import UpdateDialog
from model import Model, MotionRecordingMode
from tools.fps_counter import FpsCounter
from tools.updater import get_latest_release_gitlab
from views.main_view import View
from views.stream_connecting_view import StreamConnectingDialog


class Factory:
    def __init__(self, config: ConfigParser) -> None:
        self.config = config
        self.frameInterval = 1 / 30

    def getModel(self) -> Model:
        motionDetector = self.getMotionDetector()
        nightVisionDetector = self.getNightVisionDetector()
        contours = self.getContours()
        motionRecordingMode = MotionRecordingMode[self.config.get('recording', 'recordingMode')]
        motionWindow = int(self.config.get('controls', 'motionWindow'))
        printTime = self.config.get('controls', 'printTime') == 'True'
        return Model(
            None,
            self.frameInterval,
            motionDetector,
            nightVisionDetector,
            contours,
            motionRecordingMode,
            motionWindow,
            printTime)

    def getEffect(self) -> Union[ImageEffect, None]:
        type = self.config.get('effects', 'effects')
        if type == ImageEffectTypes.Blur.name:
            strength = float(self.config.get('effects', 'strength'))
            return Blur(strength)

        elif type == ImageEffectTypes.Trailing.name:
            length = float(self.config.get('effects', 'length'))
            type = TrailingType(int(self.config.get('effects', 'type')))
            return Trailing(length / self.frameInterval, type)

        return None

    def getMotionDetectionBaseFrame(self) -> Union[MotionDetectionBaseFrame, None]:
        scaleFactor = int(self.config.get('controls', 'scaleFactor'))
        blurKernel = int(self.config.get('controls', 'blurKernel'))
        return MotionDetectionBaseFrame(scaleFactor, blurKernel)

    def getMotionDetector(self) -> Union[MotionDetector, None]:
        detectorType = self.config.get('detector', 'detector')

        if detectorType == MotionDetectorTypes.BackgroundSubtractor.name:
            threshold = int(self.config.get('backSub', 'threshold'))
            dilationKernel = int(self.config.get('backSub', 'dilationKernel'))
            learningRate = float(self.config.get('backSub', 'learningRate'))
            return BackgroundSubtractor(threshold, dilationKernel, learningRate * self.frameInterval)

        elif detectorType == MotionDetectorTypes.CustomDetector.name:
            threshold = int(self.config.get('custom', 'threshold'))
            dilationKernel = int(self.config.get('custom', 'dilationKernel'))
            baseFrameRefresh = timedelta(seconds=int(self.config.get('custom', 'baseFrameRefresh')))
            return CustomDetector(threshold, dilationKernel, baseFrameRefresh)

        return None

    def getNightVisionDetector(self) -> Union[NightVisionDetector, None]:
        if self.config.get('recording', 'onlyNightFrames') == 'True':
            return NightVisionDetector(30)

        return None

    def getContours(self) -> Union[Contours, None]:
        thresholdSize = int(self.config.get('contours', 'thresholdSize'))
        type = self.config.get('contours', 'type')
        width = int(self.config.get('contours', 'width'))
        return Contours(thresholdSize, type, width)

    def getRecorder(self, resolution: tuple) -> Recorder:
        section = 'recording'

        fps = int(self.config.get(section, 'fps'))
        folderToSave = self.config.get(section, 'folderToSave')
        filePrefix = self.config.get(section, 'filePrefix')
        recordingSpeed = int(self.config.get(section, 'recordingSpeed'))
        splitByDays = self.config.get(section, 'splitByDays') == 'True'
        return Recorder(
            resolution,
            fps,
            folderToSave,
            filePrefix,
            recordingSpeed,
            splitByDays)

class Controller:
    updateViewEvent = None
    recordingEvent = None

    def __init__(self, config: ConfigParser) -> None:
        self.config = config
        self.config.add_callback(self.setParam)

        self.factory = Factory(self.config)
        self.model = self.factory.getModel()
        self.view = View()
        self.connectingDialog = StreamConnectingDialog()

    def setParam(self, section, key, value):
        if section == 'controls':
            if key == 'speedUp':
                if not self.model.frameProvider:
                    return
                self.model.frameProvider.speedUp = int(value)
            if key == 'scaleFactor':
                if not self.model.frameProvider or not self.model.frameProvider.motionDetectionBaseFrame:
                    return
                self.model.frameProvider.motionDetectionBaseFrame.setScaleFactor(int(value))

            elif key == 'blurKernel':
                if not self.model.frameProvider or not self.model.frameProvider.motionDetectionBaseFrame:
                    return
                self.model.frameProvider.motionDetectionBaseFrame.setKernel(int(value))

            elif key == 'printTime':
                self.model.printTime = value

        elif section == 'recording':
            if key == 'onlyNightFrames':
                self.model.nightVisionDetector = self.factory.getNightVisionDetector()

        elif self.model.motionDetector and (section == 'backSub' or section == 'custom'):
            if key == 'threshold':
                self.model.motionDetector.setThreshold(int(value))
            elif key == 'learningRate':
                self.model.motionDetector.setLearningRate(float(value) * self.model.frameInterval)
            elif key == 'dilationKernel':
                self.model.motionDetector.setDilationKernel(int(value))
            elif key == 'baseFrameRefresh':
                self.model.motionDetector.setBaseFrameRefreshTime(timedelta(seconds=int(value)))

        elif section == 'detector':
            self.setMotionDetector(value)
        elif section == 'contours':
            self.model.contours = self.factory.getContours()
        elif section == 'effects':
            self.setEffectsParams(key, value)

    def setMotionDetector(self, type: str):
        self.model.motionDetector = self.factory.getMotionDetector()

        if type == MotionDetectorTypes.BackgroundSubtractor.name:
            self.view.ids['contoursView'].disabled = False
        elif type == MotionDetectorTypes.CustomDetector.name:
            self.view.ids['contoursView'].disabled = False
        else:
            self.model.contours = None
            self.view.ids['contoursView'].disabled = True

        if self.model.motionDetector is not None:
            self.model.contours = self.factory.getContours()

    def setEffectsParams(self, key: str, value: Union[float, int, str]):
        if self.model.frameProvider is None:
            return

        if key == 'effects' or key == 'type':
            self.model.frameProvider.imageEffect = self.factory.getEffect()
        elif key == 'strength':
            self.model.frameProvider.imageEffect.setStrength(float(value))
        elif key == 'length':
            self.model.frameProvider.imageEffect.setFramesToClear(int(value) / self.model.frameInterval)

    def updateStatusBar(self):
        self.view.ids['statusBar'].update(
            self.model.frameProvider.resolution,
            self.model.frameProvider.fps,
            self.model.frameProvider.fpsCounter.averageFps,
            self.model.fpsCounter.averageFps,
            self.model.frameProvider.fpsCounter.frameId,
            self.model.fpsCounter.frameId,
            len(self.model.contours.contours) if self.model.contours and self.model.motionDetector else 0
        )

    def displayFrame(self, name: str, state: bool):
        self.view.ids['overlappingView'].ids['framesView'].add(name, state)

    def updateView(self, dt):
        if self.model.frameProvider:
            if self.model.frameProvider.motionDetectionBaseFrame:
                self.view.ids['overlappingView'].ids['framesView'].show('blured', self.model.frameProvider.motionDetectionBaseFrame.frame)

            if self.model.motionDetector:
                self.view.ids['overlappingView'].ids['framesView'].show('diff', self.model.motionDetector.deltaFrame)

            if self.model.resultFrame is not None:
                self.view.ids['overlappingView'].ids['framesView'].show('output', self.model.resultFrame)

            if self.model.nightVisionDetector:
                self.view.ids['overlappingView'].ids['framesView'].show('night', self.model.nightVisionDetector.greenBlueFrame)

            if (isinstance(self.model.frameProvider, File) or
                isinstance(self.model.frameProvider, FileList)):
                self.view.ids['playbackControls'].updateValues(
                    self.model.frameProvider.position,
                    self.model.frameProvider.timePosition
                )

        self.updateStatusBar()

    def selectStream(self, url: Union[int, str]):
        self.stopSource()

        self.model = self.factory.getModel()
        self.model.frameProvider = Stream(
            url=url,
            motionDetectionBaseFrame=self.factory.getMotionDetectionBaseFrame(),
            imageEffect=self.factory.getEffect())
        self.model.frameInterval = 1.0 / 30
        self.model.processingFrameInterval = 1.0 / 30
        self.model.frameProvider.onConnecting = self.sourceConnecting
        self.model.frameProvider.onConnected = self.sourceConnected

        self.model.frameProvider.start()
        self.view.ids['topBar'].title = 'Camera' if url == 0 else url
        self.startModel()

    def selectFile(self, path: str, startTime: datetime, frameStep: float):
        self.stopSource()

        self.model = self.factory.getModel()
        self.model.frameInterval = frameStep
        self.model.processingFrameInterval = 0
        self.view.ids['playbackControls'].disabled = False

        self.model.frameProvider = File(
            path=path,
            frameStep=timedelta(seconds=frameStep),
            startDateTime=startTime,
            motionDetectionBaseFrame=self.factory.getMotionDetectionBaseFrame(),
            imageEffect=self.factory.getEffect(),
            speedUp=int(self.config.get('controls', 'speedUp')))

        # reloadVideo on End
        self.model.frameProvider.onConnecting = self.sourceConnecting
        self.model.frameProvider.onConnected = self.sourceConnected

        self.view.ids['playbackControls'].disabled = False
        self.view.ids['topBar'].title = path
        self.startModel()

    def selectFolder(self, path: str, startTime: datetime, frameStep: float):
        self.stopSource()

        self.model = self.factory.getModel()
        self.model.frameInterval = frameStep
        self.model.processingFrameInterval = 0
        self.view.ids['playbackControls'].disabled = False

        self.model.frameProvider = FileList(
            path=path,
            frameStep=timedelta(seconds=frameStep),
            startDateTime=startTime,
            motionDetectionBaseFrame=self.factory.getMotionDetectionBaseFrame(),
            speedUp=int(self.config.get('controls', 'speedUp')))

        # reloadVideo on End
        self.model.frameProvider.onConnecting = self.sourceConnecting
        self.model.frameProvider.onConnected = self.sourceConnected

        self.view.ids['playbackControls'].disabled = False
        self.view.ids['topBar'].title = path
        self.startModel()

    def startModel(self):
        self.view.ids['overlappingView'].ids['recordButton'].disabled = False
        self.view.ids['statusBar'].disabled = False
        self.resume()

    def pause(self):
        if self.updateViewEvent:
            self.updateViewEvent.cancel()
        if self.model:
            self.model.stop()
        self.view.ids['playbackControls'].ids['playPause'].icon = 'play'

    def resume(self):
        self.updateViewEvent = Clock.schedule_interval(self.updateView, 1.0 / 30)
        if self.model:
            self.model.start()
        self.view.ids['playbackControls'].ids['playPause'].icon = 'pause'

    def stopSource(self, *args):
        self.view.ids['topBar'].title = 'Select Source'

        if self.updateViewEvent:
            self.updateViewEvent.cancel()

        self.pause()
        if self.model and self.model.frameProvider:
            self.model.frameProvider.stop()

        self.setRecordingState(False)

        self.view.ids['playbackControls'].disabled = True
        self.view.ids['overlappingView'].ids['recordButton'].disabled = True
        self.view.ids['statusBar'].disabled = True
        self.view.ids['overlappingView'].ids['framesView'].clearAll()

    def sourceConnecting(self):
        Clock.schedule_once(self.connectingDialog.open)

    def sourceConnected(self):
        Clock.schedule_once(self.connectingDialog.dismiss)

    def fileEnded(self):
        Clock.schedule_once(self.stopSource)

    def setRecordingState(self, state: bool):
        if state:
            if (isinstance(self.model.frameProvider, File) or
                isinstance(self.model.frameProvider, FileList)):
                self.view.ids['playbackControls'].setPosition(0)
                self.model.frameProvider.onConnecting = self.fileEnded

            # update effects
            self.setMotionDetector(self.config.get('detector', 'detector'))
            self.model.frameProvider.imageEffect = self.factory.getEffect()
            self.model.recorder = self.factory.getRecorder(self.model.frameProvider.resolution)

            self.view.ids['overlappingView'].ids['recordButton'].text = 'STOP ({}x, {})'.format(
                self.model.recorder.recordingSpeed,
                'Split by days' if self.model.recorder.splitByDays else 'No split')

            #self.recordingEvent = Clock.schedule_interval(self.model.recordFrame, 1.0 / self.model.recorder.fps)
        else:
            #if self.recordingEvent:
            #    self.recordingEvent.cancel()
            if self.model:
                if self.model.recorder:
                    self.model.recorder.closeFile()
                self.model.recorder = None
            self.view.ids['overlappingView'].ids['recordButton'].state = 'normal'

    def checkUpdate(self):
        if random.randint(0, 10) == 0:
            newVersion, url = get_latest_release_gitlab()
            if newVersion != App.get_running_app().version:
                updDialog = UpdateDialog()
                updDialog.show(newVersion, url)

    def showAbout(self):
        aboutDialog = AboutDialog()
        aboutDialog.show()