import cv2 as cv

from detectors.motion_detector import MotionDetector
from effects.motion_detection_base_frame import MotionDetectionBaseFrame


class BackgroundSubtractor(MotionDetector):
    def __init__(self,
            threshold: int,
            dilationKernel: int,
            learningRate: float) -> None:
        super().__init__(dilationKernel)

        self.backgroundSubtractor = cv.createBackgroundSubtractorMOG2(
            history=30,
            varThreshold=threshold,
            detectShadows=True)

        self.setLearningRate(learningRate)

    def calcDeltaFrame(self, frame) -> None:
        self.deltaFrame2 = self.backgroundSubtractor.apply(frame, learningRate=self.learningRate)
        if self.dilationKernel.size != 0:
            self.deltaFrame2 = cv.dilate(self.deltaFrame2, self.dilationKernel, iterations=2)
        self.deltaFrame = self.deltaFrame2
        self.fpsCounter.update()

    def setThreshold(self, value: int):
        self.backgroundSubtractor.setVarThreshold(value)
        print('threshold', self.backgroundSubtractor.getVarThreshold())

    def setLearningRate(self, value: float):
        self.learningRate = value
        print('learningRate', value)

if __name__ == '__main__':
    from input.stream import Stream
    from tools.preview import Preview

    stream = Stream(
        # url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=MotionDetectionBaseFrame(
            scaleFactor=1,
            kernel=21),
        imageEffect=None)

    preview = Preview('BackSub')

    motionDetector = BackgroundSubtractor(
        threshold=15,
        dilationKernel=0,
        learningRate=0.01)

    try:
        while True:
            if stream.frame is not None and stream.motionDetectionBaseFrame:
                baseFrame = stream.motionDetectionBaseFrame.frame
                if baseFrame is not None:
                    motionDetector.calcDeltaFrame(baseFrame)
                    preview.showFrame(
                        motionDetector.deltaFrame,
                        stream.resolution,
                        int(motionDetector.fpsCounter.averageFps))
    except KeyboardInterrupt:
        pass
    stream.stop()
