from enum import Enum

import cv2 as cv


class ContoursTypes(Enum):
    Empty = 0
    Precise = 1
    Rectangle = 2,
    Convex = 3

class Contours:
    def __init__(self,
            thresholdSize: int,
            type: str,
            width: int) -> None:
        self.contours = []
        self.contoursColor = (0, 255, 0)

        self.filteredContours = []
        self.filteredContoursColor = (0, 160, 255)

        self.thresholdSize = thresholdSize
        self.type = type
        self.width = width

    def hasContours(self, frame: cv.Mat) -> bool:
        self.contours = cv.findContours(frame, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)[0]

        if self.thresholdSize > 0:
            self.filterBySize()
        return len(self.contours) > 0

    def filterBySize(self):
        if self.thresholdSize > 0:
            new_contours = []
            self.filteredContours = []

            for contour in self.contours:
                (_, _, w, h) = cv.boundingRect(contour)
                if (w * h) > self.thresholdSize * self.thresholdSize:
                    new_contours.append(contour)
                else:
                    self.filteredContours.append(contour)

            self.contours = new_contours

    def getBoundRectangle(self, contours):
        rect = [999999, 999999, -1, -1]
        for contour in contours:
            (x, y, w, h) = cv.boundingRect(contour)
            rect[0] = min(rect[0], x + 1)
            rect[1] = min(rect[1], y + 1)
            rect[2] = max(rect[2], x + w - 1)
            rect[3] = max(rect[3], y + h - 1)
        return rect

    def drawContours(self, frame: cv.Mat, scale: int):
        if self.width == 0:
            return

        if self.type == ContoursTypes.Precise.name:
            if scale == 1:
                cv.drawContours(frame, self.contours, -1, self.contoursColor, self.width)
                cv.drawContours(frame, self.filteredContours, -1, self.filteredContoursColor, self.width)
            else:
                for contour in self.contours:
                    contour *= scale
                    cv.drawContours(frame, [contour], -1, self.contoursColor, self.width)
                for contour in self.filteredContours:
                    contour *= scale
                    cv.drawContours(frame, [contour], -1, self.filteredContoursColor, self.width)
        elif self.type == ContoursTypes.Rectangle.name:
            rect = self.getBoundRectangle(self.contours)
            rect *= scale
            cv.rectangle(frame, (rect[0], rect[1]), (rect[2], rect[3]), self.contoursColor, self.width)
        elif self.type == ContoursTypes.Convex.name:
            for contour in self.contours:
                hull = cv.convexHull(contour)
                if scale > 1:
                    hull *= scale
                cv.drawContours(frame, [hull], -1, self.contoursColor, self.width)
        else:
            pass

if __name__ == '__main__':
    from effects.motion_detection_base_frame import MotionDetectionBaseFrame
    from input.stream import Stream
    from tools.fps_counter import FpsCounter
    from tools.preview import Preview

    stream = Stream(
        # url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=MotionDetectionBaseFrame(
            scaleFactor=1,
            kernel=21),
        imageEffect=None)

    contours = Contours(
        thresholdSize=10,
        type=ContoursTypes.Precise.name,
        width=2)
    preview = Preview('Contours')
    fpsCounter = FpsCounter(10)

    try:
        while True:
            if stream.frame is not None and stream.motionDetectionBaseFrame:
                frame = stream.frame
                baseFrame = stream.motionDetectionBaseFrame.frame

                if baseFrame is not None:
                    thresholdFrame = cv.threshold(baseFrame, 120, 255, cv.THRESH_BINARY)[1]

                    if contours.hasContours(thresholdFrame):
                        contours.drawContours(frame, scale=stream.motionDetectionBaseFrame.scaleFactor)

                    fpsCounter.update()
                    preview.showFrame(frame,
                        stream.resolution,
                        '{} {}'.format(int(fpsCounter.averageFps), len(contours.contours)))
    except KeyboardInterrupt:
        pass
    stream.stop()
