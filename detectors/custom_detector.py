from datetime import datetime, timedelta
from typing import Optional

import cv2 as cv

from detectors.motion_detector import MotionDetector


class CustomDetector(MotionDetector):
    def __init__(self,
            threshold: int,
            dilationKernel: int,
            baseFrameRefreshTime: timedelta) -> None:
        super().__init__(dilationKernel)
        self.setThreshold(threshold)
        self.setBaseFrameRefreshTime(baseFrameRefreshTime)

        self.baseFrame: Optional[cv.Mat] = None
        self.baseFrameDateTime: Optional[datetime] = None

    def calcDeltaFrame(self, frame) -> None:
        if self.baseFrame is None or self.baseFrame.shape != frame.shape:
            self.baseFrame = frame
            self.deltaFrame = frame
            self.baseFrameDateTime = datetime.now()
            return

        self.deltaFrame = cv.absdiff(self.baseFrame, frame)
        self.deltaFrame = cv.threshold(self.deltaFrame, self.threshold, 255, cv.THRESH_BINARY)[1]
        if self.dilationKernel.size != 0:
            self.deltaFrame = cv.dilate(self.deltaFrame, self.dilationKernel, iterations=2)

        if ((self.baseFrameDateTime is not None) and
            (datetime.now() - self.baseFrameDateTime > self.baseFrameRefreshTime)):
            self.baseFrame = frame
            self.baseFrameDateTime = datetime.now()
        self.fpsCounter.update()

    def setThreshold(self, value: int):
        self.threshold = value
        print('threshold', self.threshold)

    def setBaseFrameRefreshTime(self, value: timedelta):
        self.baseFrameRefreshTime = value
        print('baseFrameRefreshTime', self.baseFrameRefreshTime)

if __name__ == '__main__':
    from effects.motion_detection_base_frame import MotionDetectionBaseFrame
    from input.stream import Stream
    from tools.preview import Preview

    stream = Stream(
        # url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=MotionDetectionBaseFrame(
            scaleFactor=1,
            kernel=17),
        imageEffect=None)
    preview = Preview('CustomDetector')

    motionDetector = CustomDetector(
        threshold=15,
        dilationKernel=0,
        baseFrameRefreshTime=timedelta(seconds=0.5))

    try:
        while True:
            if stream.frame is not None and stream.motionDetectionBaseFrame:
                baseFrame = stream.motionDetectionBaseFrame.frame
                if baseFrame is not None:
                    motionDetector.calcDeltaFrame(baseFrame)
                    preview.showFrame(motionDetector.deltaFrame,
                        stream.resolution,
                        int(motionDetector.fpsCounter.averageFps))
    except KeyboardInterrupt:
        pass
    stream.stop()
