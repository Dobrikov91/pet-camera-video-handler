from abc import ABC, abstractmethod
from enum import Enum
from typing import Optional

import cv2 as cv
import numpy as np

from tools.fps_counter import FpsCounter


class MotionDetectorTypes(Enum):
    Empty = -1
    BackgroundSubtractor = 0
    CustomDetector = 1

class MotionDetector(ABC):
    def __init__(self, dilationKernel: int) -> None:
        super().__init__()
        self.setDilationKernel(dilationKernel)

        self.fpsCounter = FpsCounter(10)
        self.deltaFrame: Optional[cv.Mat] = None
        print('MotionDetector created')

    @abstractmethod
    def calcDeltaFrame(self, frame) -> bool:
        pass

    def setDilationKernel(self, value: int):
        self.dilationKernel = np.ones((value, value), np.uint8)
        print('dilationKernel', self.dilationKernel.size)
