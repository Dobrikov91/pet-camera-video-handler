from typing import Optional

import cv2 as cv
import numpy as np


class NightVisionDetector:
    def __init__(self, threshold: int) -> None:
        self.threshold = threshold
        self.maxDiff: int = 0
        self.greenBlueFrame: Optional[cv.Mat] = None

    def isNight(self, frame: cv.Mat) -> bool:
        blue = frame[:,:,0]
        green = frame[:,:,1]
        self.greenBlueFrame = cv.absdiff(blue, green)
        #thresholdImage = cv.threshold(diffFrame, self.threshold, 255, cv.THRESH_BINARY)[1]

        self.maxDiff = np.amax(self.greenBlueFrame)
        return self.maxDiff < self.threshold

if __name__ == '__main__':
    from input.stream import Stream
    from tools.fps_counter import FpsCounter
    from tools.preview import Preview

    stream = Stream(
        # url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=None,
        imageEffect=None)

    fpsCounter = FpsCounter(10)
    preview = Preview('NightVision')

    nightVision = NightVisionDetector(20)

    try:
        while True:
            if stream.frame is not None:
                night = nightVision.isNight(stream.frame)
                fpsCounter.update()
                preview.showFrame(
                    nightVision.greenBlueFrame,
                    stream.resolution,
                    "{} {} {}".format(night, nightVision.maxDiff, int(fpsCounter.averageFps)))
    except KeyboardInterrupt:
        pass
    stream.stop()
