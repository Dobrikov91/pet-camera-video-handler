import cv2 as cv
import numpy as np

from effects.image_effect import ImageEffect


class Blur(ImageEffect):
    def __init__(self, strength: float) -> None:
        super().__init__()
        self.setStrength(strength)

    def apply(self, frame: cv.Mat) -> None:
        if self.frame is None or self.frame.shape != frame.shape:
            self.frame = frame.astype(np.single)

        self.frame = cv.addWeighted(
            self.frame, self.strength,
            frame.astype(np.single), 1.0 - self.strength,
            0.0)

    def setStrength(self, strength: float) -> None:
        assert(0.0 <= strength <= 1.0)
        self.strength = strength
        print('Blur strength', self.strength)


if __name__ == '__main__':
    from input.stream import Stream
    from tools.fps_counter import FpsCounter
    from tools.preview import Preview

    stream = Stream(
        # url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=None,
        imageEffect=None)

    preview = Preview('Blur')
    blur = Blur(0.8)
    fpsCounter = FpsCounter(10)

    try:
        while True:
            if stream.frame is not None:
                blur.apply(stream.frame)
                fpsCounter.update()
                preview.showFrame(blur.getFrame(), stream.resolution, int(fpsCounter.averageFps))
    except KeyboardInterrupt:
        pass
    stream.stop()
