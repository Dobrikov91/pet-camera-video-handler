from abc import ABC, abstractmethod
from enum import Enum
from typing import Optional

import cv2 as cv
import numpy as np


class ImageEffectTypes(Enum):
    Empty = -1
    Blur = 0
    Trailing = 1

class ImageEffect(ABC):
    def __init__(self) -> None:
        super().__init__()
        self.frame: Optional[np.ndarray] = None

    @abstractmethod
    def apply(self, frame: cv.Mat) -> None:
        pass

    def getFrame(self) -> Optional[cv.Mat]:
        if self.frame is not None:
            return self.frame.astype(np.uint8)
