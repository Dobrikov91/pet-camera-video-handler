from threading import Lock
from typing import Optional

import cv2 as cv

from effects.image_effect import ImageEffect


class MotionDetectionBaseFrame(ImageEffect):
    def __init__(self, scaleFactor: int, kernel: int) -> None:
        super().__init__()
        self.lock = Lock()

        self.setScaleFactor(scaleFactor)
        self.setKernel(kernel)

    def apply(self, frame: cv.Mat) -> None:
        with self.lock:
            newSize = (frame.shape[1] // self.scaleFactor, frame.shape[0] // self.scaleFactor)
            frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

            if self.scaleFactor > 1:
                frame = cv.resize(frame, newSize, interpolation = cv.INTER_AREA)

            if self.kernel > 1:
                self.frame = cv.GaussianBlur(
                    frame,
                    (self.kernel, self.kernel),
                    0)
            else:
                self.frame = frame

    def setScaleFactor(self, scaleFactor: int) -> None:
        self.scaleFactor = scaleFactor
        print('Scale factor', self. scaleFactor)

    def setKernel(self, kernel: int) -> None:
        assert(kernel % 2 == 1)
        self.kernel = kernel
        print('MD base frame blur kernel', self.kernel)

if __name__ == '__main__':
    from input.stream import Stream
    from tools.preview import Preview

    stream = Stream(
        #url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=MotionDetectionBaseFrame(
            scaleFactor=1,
            kernel=21),
        imageEffect=None)
    preview = Preview('MD Base frame')

    try:
        while True:
            if stream.frame is not None and stream.motionDetectionBaseFrame:
                frame = stream.motionDetectionBaseFrame.frame
                if frame is not None:
                    preview.showFrame(
                        frame,
                        stream.resolution,
                        int(stream.fpsCounter.averageFps))
    except KeyboardInterrupt:
        pass
    stream.stop()