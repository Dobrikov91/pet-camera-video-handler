from enum import Enum
from threading import Lock

import cv2 as cv
import numpy as np

from effects.image_effect import ImageEffect


class TrailingType(Enum):
    White = 0
    Black = 1
    Both = 2

class Trailing(ImageEffect):
    def __init__(self, framesToClear: float, type: TrailingType) -> None:
        super().__init__()
        self.lock = Lock()
        self.setFramesToClear(framesToClear)
        self.setType(type)
        self.whiteFrame = np.ones(1)

    def apply(self, frame: cv.Mat) -> None:
        floatFrame = frame.astype(np.single)

        if self.frame is None or self.frame.shape != frame.shape:
            self.frame = floatFrame
            self.minimum = floatFrame
            self.maximum = floatFrame

        with self.lock:
            if self.whiteFrame.shape != frame.shape:
                self.whiteFrame = np.ones(frame.shape).astype(np.single) * (255.0 / self.framesToClear)

            if self.type == TrailingType.Both:
                self.minimum = cv.min(self.minimum + self.whiteFrame, floatFrame)
                self.maximum = cv.max(self.maximum - self.whiteFrame, floatFrame)
                self.frame = cv.addWeighted(self.minimum, 0.5, self.maximum, 0.5, 0.0)
            elif self.type == TrailingType.White:
                self.frame = cv.max(self.frame - self.whiteFrame, floatFrame)
            else:
                self.frame = cv.min(self.frame + self.whiteFrame, floatFrame)

    def setFramesToClear(self, framesToClear: float):
        with self.lock:
            self.framesToClear = framesToClear
            self.whiteFrame = np.ones(1)
            print('Trailing frames to clear', self.framesToClear)

    def setType(self, type: TrailingType):
        with self.lock:
            self.type = type
            self.whiteFrame = np.ones(1)
            print('Trailing type', self.type)

if __name__ == '__main__':
    from input.stream import Stream
    from tools.fps_counter import FpsCounter
    from tools.preview import Preview

    stream = Stream(
        # url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=None,
        imageEffect=None)

    preview = Preview('Trailing')
    trailing = Trailing(3 * stream.fps, type=TrailingType.Black) # 3 sec
    fpsCounter = FpsCounter(10)

    try:
        while True:
            if stream.frame is not None:
                trailing.apply(stream.frame)
                fpsCounter.update()
                preview.showFrame(trailing.getFrame(), stream.resolution, int(fpsCounter.averageFps))
    except KeyboardInterrupt:
        pass
    stream.stop()
