from effects.motion_detection_base_frame import MotionDetectionBaseFrame
from effects.image_effect import ImageEffect
from views.android_camera_view import AndroidSource

from input.frame_provider import FrameProvider


class Android(FrameProvider):
    def __init__(self,
            source: AndroidSource,
            motionDetectionBaseFrame: MotionDetectionBaseFrame = None,
            imageEffect: ImageEffect = None) -> None:
        super().__init__('AndroidCamera', motionDetectionBaseFrame, imageEffect)

        self.source = source
        self.resolution = (640, 480)
        self.fps = 25
