from datetime import datetime, timedelta
from typing import Optional

import cv2 as cv

from effects.image_effect import ImageEffect
from effects.motion_detection_base_frame import MotionDetectionBaseFrame
from input.frame_provider import FrameProvider


class File(FrameProvider):
    def __init__(self,
            path: str,
            frameStep: timedelta,
            startDateTime: datetime,
            motionDetectionBaseFrame: Optional[MotionDetectionBaseFrame],
            imageEffect: Optional[ImageEffect] = None,
            speedUp: int = 1) -> None:
        super().__init__(path, motionDetectionBaseFrame, imageEffect)

        self.path = path
        self.source = cv.VideoCapture(path)
        self.getResolution()
        self.fps = self.source.get(cv.CAP_PROP_FPS)
        self.totalFrames = self.source.get(cv.CAP_PROP_FRAME_COUNT)
        self.length = int(self.totalFrames / self.fps)
        self.position = 0
        self.timePosition = datetime(year=1900, month=1, day=1, hour=0, minute=0, second=0)
        self.speedUp = speedUp

        self.source.release()
        self.source = None

        self.frameStep = frameStep
        self.startDateTime = startDateTime
        self.frameDateTime = self.startDateTime
        self.relativePosition = 0 # for file list

    def getResolution(self) -> tuple:
        if not self.source:
            self.source = cv.VideoCapture(self.path)

        self.resolution = (int(self.source.get(cv.CAP_PROP_FRAME_WIDTH)),
                            int(self.source.get(cv.CAP_PROP_FRAME_HEIGHT)))
        return self.resolution

    def seek(self, position: float) -> None:
        if not self.source:
            self.source = cv.VideoCapture(self.path)

        self.source.set(cv.CAP_PROP_POS_FRAMES, position * self.totalFrames)
        self.position = position
        self.updateTimePosition()

    def getFrame(self) -> Optional[cv.Mat]:
        if not self.source:
            self.source = cv.VideoCapture(self.path)

        if not self.source.isOpened():
            return None

        if self.speedUp > 1:
            self.seek(self.position + (self.speedUp - 1) / self.totalFrames)

        ret, frame = self.source.read()
        if not ret:
            self.frameDateTime = self.startDateTime
            self.seek(0)
            self.reconnect()
            return None

        if self.inConnecting:
            self.inConnecting = False

            if self.onConnected:
                self.onConnected()

        self.frameDateTime += 1 * self.frameStep
        self.position += 1 / self.totalFrames
        self.updateTimePosition()

        if self.motionDetectionBaseFrame:
            self.motionDetectionBaseFrame.apply(frame)

        if self.imageEffect:
            self.imageEffect.apply(frame)

        self.fpsCounter.update()
        return frame

    def updateTimePosition(self) -> None:
        self.timePosition = datetime(year=1900, month=1,
            day=1 + int(self.length * self.position) // (24 * 60 * 60),
            hour=int((self.length * self.position) // 3600) % 24,
            minute=int((self.length * self.position) // 60) % 60,
            second=int(self.length * self.position) % 60)
        self.frameDateTime = self.startDateTime + self.frameStep * int(self.totalFrames * self.position)