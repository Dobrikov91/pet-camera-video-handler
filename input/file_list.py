import glob
from datetime import datetime, timedelta
from typing import Optional

import cv2 as cv

from effects.image_effect import ImageEffect
from effects.motion_detection_base_frame import MotionDetectionBaseFrame
from input.file import File
from input.frame_provider import FrameProvider


class FileList(FrameProvider):
    def __init__(self,
            path: str,
            frameStep: timedelta,
            startDateTime: datetime,
            motionDetectionBaseFrame: Optional[MotionDetectionBaseFrame],
            imageEffect: Optional[ImageEffect] = None,
            speedUp: int = 1) -> None:
        super().__init__(path, motionDetectionBaseFrame, imageEffect)

        self.file_list: list[str] = []

        self.resolution = (0, 0)
        self.fps = 0
        self.totalFrames = 0
        self.length = 0
        self.position = 0.0
        self.timePosition = datetime(year=1900, month=1, day=1, hour=0, minute=0, second=0)

        self.speedUp = speedUp
        self.frameStep = frameStep
        self.startDateTime = startDateTime
        self.frameDateTime = self.startDateTime

        # .mp4 .mov .avi, .mkv .wmv .webm .flv
        patterns = [
            '/**/*.[mM][pP]4',
            '/**/*.[mM][oO][vV]',
            '/**/*.[mM][kK][vV]',
            '/**/*.[aA][vV][iI]',
            '/**/*.[wW][mM][vV]',
            '/**/*.[wW][eE][bB][mM]',
            '/**/*.[fF][lL][vV]',
        ]

        file_list = []
        for pattern in patterns:
            file_list += glob.glob(path + pattern, recursive=True)
        file_list.sort()

        self.inputFiles: list[File] = []

        for file_path in file_list:
            try:
                inputFile = File(file_path, frameStep, startDateTime, motionDetectionBaseFrame, imageEffect)

                self.resolution = inputFile.resolution
                self.fps = inputFile.fps
                self.totalFrames += inputFile.totalFrames
                self.length += inputFile.length

                self.inputFiles.append(inputFile)
            except:
                print('File error:', file_path)

        tempPosition = 0
        for file in self.inputFiles:
            file.relativePosition = tempPosition
            tempPosition += file.totalFrames / self.totalFrames

        self.currentFile = self.inputFiles[0]

    def getResolution(self) -> tuple:
        return self.resolution

    def seek(self, position: float) -> None:
        self.position = position
        self.updateTimePosition()
        #self.currentFile.source.release()

        for file in self.inputFiles:
            fileLeft = file.relativePosition
            fileRight = file.relativePosition + file.totalFrames / self.totalFrames

            if fileLeft <= position and position <= fileRight:
                restPosition = position - fileLeft
                positionInFile = restPosition * self.totalFrames / file.totalFrames

                # dont reconnect if we are already in the right file
                if self.currentFile == file:
                    self.currentFile.seek(positionInFile)
                    break

                self.currentFile.source.release()
                self.currentFile = file

                self.currentFile.reconnect()
                self.currentFile.seek(positionInFile)
                break

    def endOfInput(self) -> None:
        self.frameDateTime = self.startDateTime
        self.seek(0)
        self.reconnect()

    def getFrame(self) -> Optional[cv.Mat]:
        if self.currentFile.source is None:
            self.currentFile.source = cv.VideoCapture(self.currentFile.path)

        if not self.currentFile.source.isOpened():
            return None

        if self.speedUp > 1:
            self.seek(min(self.position + (self.speedUp - 1) / self.totalFrames, 1.0))

        ret, frame = self.currentFile.source.read()
        if not ret:
            if self.inputFiles.index(self.currentFile) == len(self.inputFiles) - 1:
                self.endOfInput()
                return None

            # try to jump to next file
            self.currentFile.source.release()
            self.currentFile = self.inputFiles[
                self.inputFiles.index(self.currentFile) + 1]
            self.currentFile.reconnect()

            ret, frame = self.currentFile.source.read()
            if not ret:
                self.endOfInput()
                return None

        if self.inConnecting:
            self.inConnecting = False

            if self.onConnected:
                self.onConnected()

        if self.motionDetectionBaseFrame:
            self.motionDetectionBaseFrame.apply(frame)

        if self.imageEffect:
            self.imageEffect.apply(frame)

        self.frameDateTime += self.frameStep
        self.position += 1 / self.totalFrames
        self.updateTimePosition()

        self.fpsCounter.update()
        return frame

    def updateTimePosition(self) -> None:
        self.timePosition = datetime(year=1900, month=1,
            day=1 + int(self.length * self.position) // (24 * 60 * 60),
            hour=int((self.length * self.position) // 3600) % 24,
            minute=int((self.length * self.position) // 60) % 60,
            second=int(self.length * self.position) % 60)
        self.frameDateTime = self.startDateTime + self.frameStep * int(self.totalFrames * self.position)