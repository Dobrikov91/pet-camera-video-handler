from abc import ABC
from datetime import datetime
from threading import Thread
from typing import Optional, Union

import cv2 as cv

from effects.image_effect import ImageEffect
from effects.motion_detection_base_frame import MotionDetectionBaseFrame
from tools.fps_counter import FpsCounter
from tools.utils import consoleLog


class FrameProvider(ABC):
    def __init__(self,
            url: Union[int, str],
            motionDetectionBaseFrame: Optional[MotionDetectionBaseFrame],
            imageEffect: Optional[ImageEffect] = None,
            connectingCallback = None,
            connectedCallback = None) -> None:
        super().__init__()

        self.url = url
        self.source = None
        self.resolution = None
        self.fps = None
        self.fpsCounter = FpsCounter(10)

        self.frame: Optional[cv.Mat] = None
        self.motionDetectionBaseFrame = motionDetectionBaseFrame
        self.imageEffect = imageEffect
        self.frameDateTime = datetime.now()

        self.thread = None
        self.running = False
        self.inConnecting = False
        self.onConnecting = connectingCallback
        self.onConnected = connectedCallback

    def __del__(self):
        self.stop()

    def reconnect(self):
        self.inConnecting = True
        if self.onConnecting:
            self.onConnecting()

        if self.source is not None:
            consoleLog('Source {} reconnecting'.format(self.url))
            self.source.release()
            self.source = cv.VideoCapture(self.url)

    def start(self):
        self.inConnecting = True
        if self.onConnecting:
            self.onConnecting()

        if not self.thread:
            self.thread = Thread(target=self.frameStream)
        self.running = True
        self.thread.start()
        consoleLog('Source {} started'.format(self.url))

    def stop(self):
        self.running = False
        if self.thread:
            self.thread.join()
        self.thread = None
        if self.source:
            self.source.release()

        self.inConnecting = False
        if self.onConnected:
            self.onConnected()
        consoleLog('Source {} stopped'.format(self.url))

    def frameStream(self):
        while self.running:
            if not self.source:
                #print('No source')
                continue

            if not self.source.isOpened():
                self.reconnect()
                continue

            res, frame = self.source.read()
            if not res:
                self.reconnect()
                continue

            if self.inConnecting:
                self.inConnecting = False
                if self.onConnected:
                    self.onConnected()

            if self.motionDetectionBaseFrame:
                self.motionDetectionBaseFrame.apply(frame)

            if self.imageEffect:
                self.imageEffect.apply(frame)

            self.frame = frame
            self.frameDateTime = datetime.now()
            self.fpsCounter.update()
            self.resolution = (self.frame.shape[1], self.frame.shape[0])

    def getFrame(self) -> Optional[cv.Mat]:
        return self.frame