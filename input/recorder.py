import os
from datetime import datetime, timedelta
from pathlib import Path
from typing import Optional

import cv2 as cv

from tools.fps_counter import FpsCounter
from tools.utils import consoleLog, getStrDateTime


class Recorder:
    def __init__(self,
            resolution: tuple,
            fps: int,
            folderToSave: str,
            filePrefix: str,
            recordingSpeed: int,
            splitByDays: bool) -> None:

        self.resolution = resolution
        self.fps = fps

        self.folderToSave = os.path.abspath(folderToSave)
        self.filePrefix = filePrefix

        self.recordingSpeed = recordingSpeed
        self.splitByDays = splitByDays
        self.length = timedelta(seconds=0)
        self.startTime: Optional[datetime] = None

        self.fpsCounter = FpsCounter(10)
        self.path: Optional[str] = None
        self.file = None

    def __del__(self) -> None:
        self.closeFile()

    def createFile(self) -> None:
        Path(self.folderToSave).mkdir(parents=True, exist_ok=True)
        consoleLog('Recorder path {}'.format(self.folderToSave))

        self.path = '{}/{}_{}.mp4'.format(
            self.folderToSave,
            self.filePrefix,
            getStrDateTime(datetime.now()).replace(':','_'))

        self.file = cv.VideoWriter(self.path,
                        cv.VideoWriter_fourcc(*'mp4v'),
                        self.fps,
                        self.resolution)

        self.startTime = datetime.now()
        consoleLog('VideoFile {} created'.format(self.path))
        consoleLog('Resolution {}, fps {}, rec. speed {}x, split by days {}'.format(
            self.resolution,
            self.fps,
            self.recordingSpeed,
            self.splitByDays))

    def closeFile(self) -> None:
        if self.file:
            self.file.release()
            consoleLog('Release file')
        self.file = None
        consoleLog('VideoFile {} closed'.format(self.path))

    def addFrame(self, frame):
        self.fpsCounter.update()
        if (self.fpsCounter.frameId - 1) % self.recordingSpeed != 0:
            return

        if self.startTime is not None and self.startTime.day != datetime.now().day:
            self.closeFile()

        if self.file is None:
            self.createFile()

        if frame.shape[0] != self.resolution[1] or frame.shape[1] != self.resolution[0]:
            frame = cv.resize(frame, self.resolution)

        if self.file:
            self.file.write(frame)
        self.length += timedelta(seconds=1.0 / self.fps)

if __name__ == '__main__':
    from input.stream import Stream
    from tools.preview import Preview

    stream = Stream(url=0, motionDetectionBaseFrame=None)
    stream.start()
    preview = Preview('Recorder')

    recorder = Recorder(
        folderToSave='.',
        filePrefix='Webcam',
        resolution=(800, 600),
        fps=30,
        recordingSpeed=3,
        splitByDays=False)

    try:
        while True:
            if stream.frame is not None:
                recorder.addFrame(stream.frame)
                preview.showFrame(
                    frame=stream.frame,
                    resolution=stream.resolution,
                    text=str(int(recorder.fpsCounter.averageFps)))
    except KeyboardInterrupt:
        pass

    consoleLog('Output file length {}'.format(recorder.length))
    stream.stop()
