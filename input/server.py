import queue
from datetime import datetime
from http.server import BaseHTTPRequestHandler, HTTPServer
from threading import Thread

from effects.motion_detection_base_frame import MotionDetectionBaseFrame
from effects.image_effect import ImageEffect
from tools.utils import consoleLog, cv2frameFromBinary

from input.frame_provider import FrameProvider

framesQueue = queue.Queue(maxsize=100)

class CustomHandler(BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super(CustomHandler, self).__init__(*args, **kwargs)

    def do_POST(self):
        length = int(self.headers['Content-Length'])
        frame = cv2frameFromBinary(self.rfile.read(length))
        framesQueue.put(frame)

        self.send_response_only(200, 'OK')
        self.send_header('Date', str(datetime.now()))
        self.send_header('Content-type','text/html')
        self.end_headers()

class QueueReader():
    def read(self):
        try:
            frame = framesQueue.get(timeout=0.5)
            return True, frame
        except queue.Empty:
            pass
        return False, None

    def release(self):
        pass

    def isOpened(self) -> bool:
        return True

class Server(FrameProvider):
    def __init__(self,
            motionDetectionBaseFrame: MotionDetectionBaseFrame = None,
            imageEffect: ImageEffect = None) -> None:
        super().__init__('Server', motionDetectionBaseFrame, imageEffect)
        self.fps = 0

        self.source = QueueReader()

        server_address = ('', 8001)
        self.httpd = HTTPServer(server_address, CustomHandler)
        self.httpThread = Thread(target=self.httpd.serve_forever)
        self.httpThread.start()
        consoleLog('HTTTPServer started in separate thread')

    def stop(self):
        consoleLog()
        super().stop()
        if self.httpd is not None:
            self.httpd.shutdown()
            consoleLog('HTTPServer stopped')
        self.httpThread.join()
        self.httpd = None

if __name__ == '__main__':
    from tools.preview import Preview

    server = Server(MotionDetectionBaseFrame(1, 3))
    preview = Preview('Server')

    try:
        while True:
            frame = server.frame
            if frame is not None:
                preview.showFrame(frame)
                print(server.resolution,
                    server.fps,
                    int(server.fpsCounter.averageFps),
                    server.fpsCounter.frameId)
    except KeyboardInterrupt:
        pass
    server.stop()
