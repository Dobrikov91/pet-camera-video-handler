from typing import Optional, Union

import cv2 as cv

from effects.image_effect import ImageEffect
from effects.motion_detection_base_frame import MotionDetectionBaseFrame
from input.frame_provider import FrameProvider


class Stream(FrameProvider):
    def __init__(self,
            url: Union[int, str],
            motionDetectionBaseFrame: Optional[MotionDetectionBaseFrame],
            imageEffect: Optional[ImageEffect] = None) -> None:
        super().__init__(url, motionDetectionBaseFrame, imageEffect)

        self.source = cv.VideoCapture(url)
        self.getResolution()
        self.fps = self.source.get(cv.CAP_PROP_FPS)

    def getResolution(self) -> tuple:
        self.resolution = (int(self.source.get(cv.CAP_PROP_FRAME_WIDTH)),
                            int(self.source.get(cv.CAP_PROP_FRAME_HEIGHT)))
        return self.resolution

    def setResolution(self, resolution: tuple) -> None:
        self.source.set(cv.CAP_PROP_FRAME_WIDTH, resolution[0])
        self.source.set(cv.CAP_PROP_FRAME_HEIGHT, resolution[1])
        self.getResolution()

if __name__ == '__main__':
    from tools.preview import Preview

    stream = Stream(
        # url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=None,
        imageEffect=None)
    preview = Preview()

    try:
        while True:
            if stream.frame is not None:
                #print(stream.fpsCounter.averageFps)
                #'''
                preview.showFrame(stream.frame, stream.resolution, "{} {} {} {}".format(
                    stream.resolution,
                    stream.fps,
                    int(stream.fpsCounter.averageFps),
                    stream.fpsCounter.frameId
                ))
                #'''
    except KeyboardInterrupt:
        pass
    stream.stop()
