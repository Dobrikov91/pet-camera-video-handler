from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.gridlayout import MDGridLayout
from kivymd.uix.label import MDLabel
from kivymd.uix.textfield import MDTextField

Builder.load_string('''
<Header@MDLabel>:
    size_hint_x: 1

<CopyableText@MDTextFieldRect>:
    size_hint_x: 3
    readonly: True

<AboutView>:
    cols: 2
    rows: 8

    size_hint_y: None
    height: dp(32 * self.rows)

    Header:
        text: 'Version'

    MDLabel:
        text: str(app.version)

    Header:
        text: 'Author'

    MDLabel:
        text: 'Sergei Dobrikov'

    Header:
        text: 'Email'

    CopyableText:
        text: 'dobrikov91@yandex.ru'

    Header:
        text: 'Telegram'

    CopyableText:
        text: '@DOB_nn'

    Header:
        text: 'Discord'

    CopyableText:
        text: 'te4#8507'

    Header:
        text: 'Donations'

    MDLabel:

    Header:
        text: 'BTC'

    CopyableText:
        text: 'bc1qkcsdzh332jxww2ceradd8u3ynvzux0a0wrxjlk'

    Header:
        text: 'Buy me a coffee'

    CopyableText:
        text: 'https://www.buymeacoffee.com/dobrikov91'
''')

class AboutView(MDGridLayout):
    pass

class AboutDialog:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.dialog = None

    def show(self):
        if not self.dialog:
            self.dialog = MDDialog(
                title='About',
                type='custom',
                content_cls=AboutView(),
                buttons=[
                    MDRectangleFlatButton(
                        text='CLOSE',
                        on_release=lambda x: self.close()
                    )
                ],
            )
        self.dialog.open()

    def close(self, *args):
        if self.dialog:
            self.dialog.dismiss()