from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.tabbedpanel import TabbedPanelItem
from kivymd.uix.behaviors.toggle_behavior import MDToggleButton
from kivymd.uix.button import MDRaisedButton, MDRectangleFlatButton

Builder.load_string('''
<ConfigToggleButton>:
    name: ''
    section: ''
    text: ''
    height: '36dp'
    size_hint_y: None

    state: 'down' if self.name and self.section and app.config.get(self.section, self.name) == 'True' else 'normal'
    on_release: app.config.set(self.section, self.name, True if self.state == 'down' else False)

<ConfigMultistateButton>:
    name: ''
    section: ''
    height: '36dp'
    size_hint_y: None

<ConfigTabbedPanelItem>:
    name: ''
    section: ''
    key: self.section
    text: self.name
    height: '36dp'
    size_hint_y: None

    on_press: app.config.set(self.section, self.key, self.name)
''')

class SimpleToggleButton(MDRectangleFlatButton, MDToggleButton):
    pass

class ConfigToggleButton(MDRectangleFlatButton, MDToggleButton):
    pass

class ConfigMultistateButton(MDRaisedButton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        Clock.schedule_once(self.getState)

    def getState(self, dt):
        self.customState = App.get_running_app().config.get(self.section, self.name)
        self.text = self.customState

    def on_release(self):
        self.customState = self.states[(self.states.index(self.customState) + 1) % len(self.states)]
        self.text = self.customState
        App.get_running_app().config.set(self.section, self.name, self.customState)
        return super().on_release()

class ConfigTabbedPanelItem(TabbedPanelItem):
    pass