from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.textfield import MDTextFieldRect

Builder.load_string('''
<UrlFieldView>:
    text: app.config.getdefault('controls', 'streamUrl', 'http://')
    hint_text: 'http://'
    size_hint_y: None
    height: '36dp'
    multiline: False
''')

class UrlFieldView(MDTextFieldRect):
    pass

class EnterUrlDialog:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.dialog = None
        self.onEnteredCallback = None

    def show(self):
        if not self.dialog:
            self.dialog = MDDialog(
                title='Enter URL',
                type='custom',
                content_cls=UrlFieldView(),
                buttons=[
                    MDRectangleFlatButton(
                        text='CANCEL',
                        on_release=self.close),
                    MDRectangleFlatButton(
                        text='OK',
                        on_release=self.entered),
                ],
            )
        self.dialog.open()

    def close(self, *args):
        if self.dialog:
            self.dialog.dismiss()

    def entered(self, *args):
        url = self.dialog.content_cls.text
        if not url:
            self.close()

        App.get_running_app().config.set('controls', 'streamUrl', url)
        if self.onEnteredCallback:
            self.onEnteredCallback(url)
        self.close()

if __name__ == "__main__":
    from kivymd.app import MDApp
    from kivymd.uix.boxlayout import MDBoxLayout

    class Test(MDApp):
        def build(self):
            self.theme_cls.theme_style = "Dark"
            EnterUrlDialog().show()
            return MDBoxLayout()

    Test().run()