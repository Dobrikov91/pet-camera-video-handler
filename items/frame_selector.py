from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.metrics import dp
from kivymd.uix.boxlayout import MDBoxLayout

from items.buttons import SimpleToggleButton
from views.frames_view import FramesView

class FrameSelectorButton(SimpleToggleButton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bind(on_release=self.on_release)
        # fix bug with background color
        self._update_bg(self, self.state)

    def on_release(self, *args):
        App.get_running_app().controller.displayFrame(self.text, self.state=='down')

class FrameSelector(MDBoxLayout):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        Clock.schedule_once(self.initButtons)

    def initButtons(self, dt):
        for name in FramesView.frameNames:
            self.add_widget(
                FrameSelectorButton(
                    text=name,
                    size_hint_x=1,
                    state='down' if name == 'output' else 'normal'))