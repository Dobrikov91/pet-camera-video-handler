from datetime import datetime

from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout

Builder.load_string('''
<PlaybackControls>:
    orientation: 'vertical'
    disabled: True
    size_hint_y: None
    height: dp(36)

    MDBoxLayout:
        orientation: 'horizontal'

        MDIconButton:
            id: playPause
            icon: 'play'
            on_release: root.playPause()
            md_bg_color: app.theme_cls.primary_color
            icon_size: dp(12)

        MDSlider:
            id: slider
            min: 0
            value: 0
            max: 1
            hint: False
            on_touch_down: if self.collide_point(*args[1].pos): root.disableSlider()
            on_touch_up: if self.collide_point(*args[1].pos): root.setPosition(self.value)

        MDLabel:
            id: videoTime
            size_hint_x: None
            width: dp(1.8 * 36)
            text: '00:00:00'
''')

class PlaybackControls(MDBoxLayout):
    sliderOnHold = False
    def disableSlider(self):
        self.sliderOnHold = True

    def playPause(self) -> None:
        if self.ids['playPause'].icon == 'play':
            App.get_running_app().controller.resume()
        else:
            App.get_running_app().controller.pause()

    def setPosition(self, position):
        App.get_running_app().controller.pause()
        try:
            App.get_running_app().controller.model.frameProvider.seek(position)
        except:
            pass
        App.get_running_app().controller.resume()
        self.sliderOnHold = False

    def updateValues(self, videoPosition: float, timePosition: datetime) -> None:
        if not self.sliderOnHold:
            self.ids['slider'].value = videoPosition
        self.ids['videoTime'].text = '{:02d}:{:02d}:{:02d}'.format(
            timePosition.hour,
            timePosition.minute,
            timePosition.second
        )

if __name__ == "__main__":
    from kivy.app import App

    class Test(App):
        def build(self):
            return PlaybackControls()

    Test().run()