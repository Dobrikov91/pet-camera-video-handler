from tkinter.filedialog import askdirectory

from kivy.app import App
from kivy.lang import Builder
from kivy.properties import StringProperty
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog

from items.buttons import ConfigToggleButton, SimpleToggleButton
from items.sliders import ConfigSliderFPS

Builder.load_string('''
<FolderChooser>:
    orientation: 'horizontal'
    spacing: dp(8)

    id: folderChooser
    name: 'folderToSave'
    folderPath: app.config.get('recording', self.name)

    TextInput:
        id: folderPathText
        size_hint_x: 3

        readonly: True
        text: root.folderPath

    MDRectangleFlatButton:
        text: 'Choose folder'
        size_hint_x: 1
        on_release: root.chooseFolder()

<StartRecordingView>:
    orientation: 'vertical'
    size_hint_y: None
    height: dp(4*36)
    spacing: dp(8)

    FolderChooser:
        id: folderChooser
        size_hint_y: None
        height: dp(36)

    ConfigSliderFPS:
        id: recordingSpeed
        section: 'recording'
        name: 'recordingSpeed'

    MDBoxLayout:
        orientation: 'horizontal'
        size_hint_y: None
        height: dp(36)
        spacing: dp(8)

        MDLabel:
            text: 'Recording mode'
            haligh: 'left'
            size_hint: 2, 1

        ConfigMultistateButton:
            size_hint_x: 1
            section: 'recording'
            name: 'recordingMode'
            states: ['Motion', 'Static', 'All']

    MDBoxLayout:
        orientation: 'horizontal'
        size_hint_y: None
        height: dp(36)
        spacing: dp(8)

        ConfigToggleButton:
            id: onlyNightFrames
            section: 'recording'
            name: 'onlyNightFrames'
            text: 'Record only night frames'
            size_hint_x: 1

        ConfigToggleButton:
            id: splitByDays
            name: 'splitByDays'
            section: 'recording'
            text: 'Split output file by days'
            size_hint_x: 1

''')

class FolderChooser(MDBoxLayout):
    folderPath = StringProperty('')

    def chooseFolder(self):
        self.folderPath = askdirectory(
            initialdir=App.get_running_app().config.get('recording', 'folderToSave'))
        if self.folderPath:
            App.get_running_app().config.set('recording', 'folderToSave', self.folderPath)

class StartRecordingView(MDBoxLayout):
    pass

class StartRecordingDialog:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.dialog = None
        self.onSelectedCallback = None
        self.onCancelCallback = None

    def show(self):
        if not self.dialog:
            self.dialog = MDDialog(
                title='Set recording params',
                type='custom',
                content_cls=StartRecordingView(),
                buttons=[
                    MDRectangleFlatButton(
                        text='CANCEL',
                        on_release=self.close),
                    MDRectangleFlatButton(
                        text='OK',
                        on_release=self.selected),
                ],
            )
        self.dialog.open()

    def close(self, *args):
        self.dialog.dismiss()
        if self.onCancelCallback:
            self.onCancelCallback()

    def selected(self, *args):
        self.dialog.dismiss()
        if self.onSelectedCallback:
            self.onSelectedCallback()

class RecordButton(StartRecordingDialog, SimpleToggleButton):
    def on_state(self, *args):
        if self.state == 'down':
            self.onSelectedCallback = lambda: App.get_running_app().controller.setRecordingState(True)
            self.onCancelCallback = self.unpress
            self.show()
        else:
            App.get_running_app().controller.setRecordingState(False)
            self.text = 'RECORD'

    def unpress(self):
        self.state = 'normal'

if __name__ == "__main__":
    from kivymd.app import MDApp
    from kivymd.uix.boxlayout import MDBoxLayout

    class Test(MDApp):
        def build(self):
            self.theme_cls.theme_style = "Dark"
            return RecordButton()

    Test().run()