from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.slider import MDSlider

Builder.load_string('''
<SliderBase>:
    name: ''
    section: ''
    suffix: ''
    customValue: self.value
    valueText: '{:.2f} {}'.format(self.customValue, self.suffix)
    max: 1000 # workaround for sliders with max greater than 100

    hint: False
    size_hint_y: None
    height: dp(36)

    MDRelativeLayout:
        #md_bg_color: 0, 1, 0, 0.5
        size: self.parent.size
        pos: self.parent.pos

        MDBoxLayout:
            #md_bg_color: 0, 1, 0, 0.5
            orientation: 'horizontal'
            padding: dp(8), dp(0)

            MDLabel:
                #md_bg_color: 1, 0, 0, 0.5
                adaptive_size: True
                pos_hint: {'top': 1.1, 'left': 1}
                text: root.name

            MDLabel:
                #md_bg_color: 0, 0, 1, 0.5
                adaptive_height: True
                halign: 'right'
                pos_hint: {'top': 1.1, 'right': 1}
                text: root.valueText

<ConfigSliderFloat>:
    value: float(self.readValue(self.section, self.name))
    on_touch_up: self.writeValue(*args[1].pos, float(self.value))

<ConfigSliderInt>:
    value: self.readValue(self.section, self.name)
    customValue: int(self.value)
    on_touch_up: self.writeValue(*args[1].pos, int(self.customValue))
    valueText: '{} {}'.format(int(self.customValue), self.suffix)

<ConfigSliderMap>:
    values: [1]
    min: 0
    max: len(self.values) - 1
    step: 1
    indexValue: int(self.readValue(self.section, self.name))
    value: self.values.index(self.indexValue) if self.indexValue in self.values else 0
    customValue: self.values[int(self.value)]
    on_touch_up: self.writeValue(*args[1].pos, int(self.customValue))

<ConfigSliderTextMap>:
    values: [1]
    min: 0
    max: len(self.values) - 1
    step: 1
    value: int(self.readValue(self.section, self.name))
    customValue: self.values[int(self.value)]
    on_touch_up: self.writeValue(*args[1].pos, int(self.value))

<ConfigSliderTime>:
    timeValue: root.customValue if root.customValue < 60 else root.customValue // 60 if root.customValue < 60 * 60 else root.customValue // (60 * 60)
    suffix: 's' if root.customValue < 60 else 'm' if root.customValue < 60 * 60 else 'h'
    valueText: '{} {}'.format(str(root.timeValue), root.suffix)

<ConfigSliderFPS>:
    values: [1, 2, 3, 5, 10, 15, 1 * 30, 2 * 30, 3 * 30, 5 * 30, 10 * 30, 15 * 30, 20 * 30, 30 * 30, 60 * 30]
    suffix: 'x ({:.2f} s)'.format(root.customValue / 30)

<ConfigSliderScale>:
    values: [1, 2, 4, 8, 16]
    suffix: 'x'
    valueText: '1/{} {}'.format(root.customValue, root.suffix)
''')

class SliderBase(MDSlider):
    def readValue(self, section, name):
        try:
            return App.get_running_app().config.get(section, name)
        except:
            return 0

    def writeValue(self, x, y, value):
        if (self.collide_point(x, y) and
            not self.disabled):
            App.get_running_app().config.set(self.section, self.name, value)

class ConfigSliderFloat(SliderBase):
    pass

class ConfigSliderInt(SliderBase):
    pass

class ConfigSliderMap(SliderBase):
    pass

class ConfigSliderTextMap(SliderBase):
    pass

class ConfigSliderTime(ConfigSliderMap):
    pass

class ConfigSliderFPS(ConfigSliderMap):
    pass

class ConfigSliderScale(ConfigSliderMap):
    pass

if __name__ == '__main__':
    from kivymd.app import MDApp

    class Test(MDApp):
        def build(self):
            self.theme_cls.theme_style = "Dark"
            return ConfigSliderFloat()

    Test().run()