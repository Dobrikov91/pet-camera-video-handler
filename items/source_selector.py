from tkinter.filedialog import askdirectory, askopenfilename

from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRectangleFlatButton, MDRectangleFlatIconButton

from items.enter_url_dialog import EnterUrlDialog
from items.video_file_params_dialog import VideoFileParamsDialog

Builder.load_string('''
<SourceSelector>:
    size_hint_x: 1
    spacing: '6dp'

    CameraButton:
        text: 'Camera'
        icon: 'camera'
        size_hint_x: 1

    StreamButton:
        text: 'Stream'
        icon: 'view-stream'
        size_hint_x: 1

    FileButton:
        text: 'File'
        icon: 'file-video'
        size_hint_x: 1

    FolderButton:
        text: 'Folder'
        icon: 'folder-play'
        size_hint_x: 1

    #MDRectangleFlatButton:
    #    text: 'Version {}'.format(app.version)
    #    size_hint_x: 0.5

    #    url: 'https://cdn.flowplayer.com/a30bd6bc-f98b-47bc-abf5-97633d4faea0/hls/de3f6ca7-2db3-4689-8160-0f574a5996ad/playlist.m3u8'
''')

class CameraButton(MDRectangleFlatIconButton):
    def on_release(self):
        App.get_running_app().controller.selectStream(0)

class StreamButton(EnterUrlDialog, MDRectangleFlatIconButton):
    def on_release(self):
        App.get_running_app().controller.stopSource()
        self.onEnteredCallback = App.get_running_app().controller.selectStream
        self.show()

class FileButton(VideoFileParamsDialog, MDRectangleFlatIconButton):
    def on_release(self):
        self.selectedCallback = App.get_running_app().controller.selectFile

        self.inputFilePath = askopenfilename(
            initialdir=App.get_running_app().config.get('controls', 'inputFolder'),
            filetypes=[('Video files', '.mp4 .mov .avi, .mkv .wmv .webm .flv')])

        if self.inputFilePath:
            App.get_running_app().controller.stopSource()
            App.get_running_app().config.set('controls', 'inputFolder', self.inputFilePath)
            self.show()

class FolderButton(VideoFileParamsDialog, MDRectangleFlatIconButton):
    def on_release(self):
        self.selectedCallback = App.get_running_app().controller.selectFolder

        self.inputFilePath = askdirectory(
            initialdir=App.get_running_app().config.get('controls', 'inputFolder'))

        if self.inputFilePath:
            App.get_running_app().controller.stopSource()
            App.get_running_app().config.set('controls', 'inputFolder', self.inputFilePath)
            self.show()

class SourceSelector(MDBoxLayout):
    pass