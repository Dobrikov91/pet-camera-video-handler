import webbrowser

from kivy.app import App
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog


class UpdateDialog:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.dialog = None

    def show(self, version, url):
        if not self.dialog:
            self.dialog = MDDialog(
                title='Update available',
                text=f'Version {version} is available. Click OK to download it.',
                buttons=[
                    MDRectangleFlatButton(
                        text='SKIP',
                        on_release=lambda x: self.close()),
                    MDRectangleFlatButton(
                        text='OK',
                        on_release=lambda x: self.openLink(url)),
                ],
            )
        self.dialog.open()

    def close(self, *args):
        if self.dialog:
            self.dialog.dismiss()

    # open link to download new version
    def openLink(self, url, *args):
        webbrowser.open(url)
        self.close()
        App.get_running_app().stop()