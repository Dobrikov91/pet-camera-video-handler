from datetime import datetime

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import BooleanProperty, NumericProperty, StringProperty
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDFlatButton, MDRectangleFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.label import MDLabel
from kivymd.uix.pickers import MDDatePicker, MDTimePicker
from kivymd.uix.textfield import MDTextFieldRect

Builder.load_string('''
<VideoFileParamsView>:
    orientation: 'horizontal'
    adaptive_height: True

    MDLabel:
        halign: 'center'
        text: 'Choose video start'

    MDRectangleFlatButton:
        id: startDate
        text: 'date'
        on_release: root.showDatePicker()

    MDRectangleFlatButton:
        id: startTime
        text: 'time'
        on_release: root.showTimePicker()

    MDLabel:
        halign: 'center'
        text: 'frame step (s)'

    MDTextFieldRect:
        id: frameStep
        hint_text: '0.5'
        multiline: False
        input_filter: 'float'
''')

class VideoFileParamsView(MDBoxLayout):
    datePicked = BooleanProperty(False)
    timePicked = BooleanProperty(False)
    frameStepPicked = BooleanProperty(False)
    allPicked = datePicked and timePicked and frameStepPicked

    startDate = datetime.now().date
    startTime = datetime.now().time
    frameStep = NumericProperty(1)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        Clock.schedule_once(lambda _: self.ids['frameStep'].bind(text=self.onSaveFrameStep))

    def onSaveDate(self, instance, value, date_range):
        self.startDate = value
        self.ids['startDate'].text = str(value)
        self.datePicked = True

    def onSaveTime(self, instance, time):
        self.startTime = time
        self.ids['startTime'].text = str(time)
        self.timePicked = True

    def onSaveFrameStep(self, instance, value):
        if not value:
            self.frameStepPicked = False
            return

        self.frameStep = float(value)
        self.frameStepPicked = True

    def showDatePicker(self):
        date_dialog = MDDatePicker()
        date_dialog.bind(on_save=self.onSaveDate)
        date_dialog.open()

    def showTimePicker(self):
        time_dialog = MDTimePicker()
        time_dialog.bind(time=self.onSaveTime)
        time_dialog.open()


class VideoFileParamsDialog():
    inputFilePath = StringProperty('')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.dialog = None
        self.videoParams = VideoFileParamsView()
        self.selectedCallback = None

    def show(self):
        if not self.dialog:
            self.dialog = MDDialog(
                text='Select video params',
                type='custom',
                auto_dismiss=False,
                content_cls=self.videoParams,
                buttons=[
                    MDFlatButton(
                        text="CANCEL",
                        on_release=self.paramsCancelled
                    ),
                    MDFlatButton(
                        text="OK",
                        on_release=self.paramsSelected
                    ),
                ],
            )
        self.dialog.open()

    def paramsSelected(self, *args):
        if self.dialog:
            self.dialog.dismiss()

        if self.selectedCallback:
            startDateTime = datetime(
                year=self.videoParams.startDate.year,
                month=self.videoParams.startDate.month,
                day=self.videoParams.startDate.day,
                hour=self.videoParams.startTime.hour,
                minute=self.videoParams.startTime.minute,
                second=0)
            self.selectedCallback(self.inputFilePath, startDateTime, self.videoParams.frameStep)

    def paramsCancelled(self, *args):
        if self.dialog:
            self.dialog.dismiss()

if __name__ == "__main__":
    from kivymd.app import MDApp

    '''
    class TestView(MDApp):
        def build(self):
            self.theme_cls.theme_style = "Dark"
            return VideoFileParamsView()

    TestView().run()
    '''

    class Test(MDApp):
        def build(self):
            self.theme_cls.theme_style = "Dark"
            VideoFileParamsDialog().show()
            return MDBoxLayout()

    Test().run()
