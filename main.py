import cProfile
import os
import sys
# NSException when creating tkroot afetr kivy
from tkinter import Tk

root = Tk()
root.withdraw()

from kivy.config import Config
from kivy.core.window import Window
from kivymd.app import MDApp

from controller import Controller
from tools.config import defaultConfig

Config.set('input', 'mouse', 'mouse,multitouch_on_demand')

class PetCamera(MDApp):
    def build_config(self, config):
        for section in defaultConfig:
            config.setdefaults(section, defaultConfig[section])

    def get_application_config(self):
        # This defines the path and name where the ini file is located
        return str(os.path.join(self.user_data_dir, 'petcamera.ini'))

    def build(self):
        self.version = '1.0.2'
        self.theme_cls.material_style = 'M3'
        self.theme_cls.theme_style = 'Dark'
        self.controller = Controller(self.config)
        Window.size = (800, 523)
        self.title = 'PetCamera {}'.format(self.version)
        return self.controller.view

    def on_start(self):
        self.profile = None # cProfile.Profile()
        if self.profile:
            self.profile.enable()
        self.controller.checkUpdate()

    def on_stop(self):
        if self.profile:
            self.profile.disable()
            self.profile.dump_stats('myapp.profile')

        self.controller.stopSource()
        self.config.write()

if __name__ == '__main__':
    PetCamera().run()
