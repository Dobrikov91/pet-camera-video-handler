from datetime import datetime, timedelta
from multiprocessing import Event
from threading import Thread
from typing import Optional

from detectors.contours import Contours, ContoursTypes
from detectors.motion_detector import MotionDetector
from detectors.night_vision_detector import NightVisionDetector
from input.frame_provider import FrameProvider
from input.recorder import Recorder
from tools.config import MotionRecordingMode
from tools.fps_counter import FpsCounter
from tools.image_print import printTimeOnImage


class Model:
    def __init__(self,
            frameProvider: Optional[FrameProvider] = None,
            frameInterval: float = 1 / 30,
            motionDetector: Optional[MotionDetector] = None,
            nightVisionDetector: Optional[NightVisionDetector] = None,
            contours: Optional[Contours] = None,
            motionRecordingMode: MotionRecordingMode = MotionRecordingMode.Motion,
            motionWindow: int = 0,
            printTime = True) -> None:
        self.frameProvider = frameProvider
        self.motionDetector = motionDetector
        self.nightVisionDetector = nightVisionDetector
        self.contours = contours
        self.recorder: Optional[Recorder] = None
        self.printTime = printTime

        self.fpsCounter = FpsCounter(10)

        self.motionRecordingMode = motionRecordingMode
        self.motionWindow = timedelta(seconds=motionWindow)
        self.noFrameCallback = None

        self.lastMotionDateTime = None
        self.recordFrames = False
        self.resultFrame = None

        self.frameInterval = frameInterval
        self.processingFrameInterval = self.frameInterval
        self.stopped = Event()
        #self.stopped = True
        self.thread = None

    def start(self) -> None:
        self.stopped = Event()
        #self.stopped = False
        self.thread = Thread(target=self.processingThread)
        self.thread.start()
        print('model started')

    def stop(self) -> None:
        self.stopped.set()
        #self.stopped = True
        if self.thread:
            self.thread.join()
        self.thread = None
        print('model stopped')

    def processingThread(self) -> None:
        prevTime = datetime.now()
        while not self.stopped.wait(
                self.processingFrameInterval -
                (datetime.now() - prevTime).seconds -
                (datetime.now() - prevTime).microseconds / 1000 / 1000 - 3.6e-2 * self.processingFrameInterval):
        #from time import sleep
        #while not self.stopped:
        #    sleep(self.frameInterval)
            prevTime = datetime.now()
            self.processFrame()

    def shouldRecordFrame(self, foundMotion: bool) -> bool:
        if self.motionRecordingMode == MotionRecordingMode.Motion:
            return foundMotion
        elif self.motionRecordingMode == MotionRecordingMode.Static:
            return not foundMotion
        elif self.motionRecordingMode == MotionRecordingMode.All:
            return True
        return False

    def processFrame(self) -> None:
        if not self.frameProvider:
            #print('No frame source')
            return

        frame = self.frameProvider.getFrame()
        if frame is None:
            if self.noFrameCallback:
                self.noFrameCallback()
            return
        self.fpsCounter.update()
        self.fpsCounter.frameId -= 1

        nightFrame = True
        if self.nightVisionDetector and not self.nightVisionDetector.isNight(frame):
            nightFrame = False

        foundMotion = True
        if self.frameProvider.motionDetectionBaseFrame and self.motionDetector and self.contours and nightFrame:
            self.motionDetector.calcDeltaFrame(self.frameProvider.motionDetectionBaseFrame.frame)

            if self.motionDetector:
                if self.motionDetector.deltaFrame is not None:
                    if not self.contours.hasContours(self.motionDetector.deltaFrame):
                        foundMotion = False

        if foundMotion:
            self.fpsCounter.frameId += 1

        #if foundMotion:
        #    self.lastMotionDateTime = self.frameProvider.frameDateTime
        #else:
        #    if ((self.lastMotionDateTime is not None) and
        #        (self.frameProvider.frameDateTime - self.lastMotionDateTime > self.motionWindow)):
        #        self.recordFrames = False

        self.recordFrames = self.shouldRecordFrame(foundMotion)

        if not nightFrame:
            self.recordFrames = False

        if self.frameProvider.imageEffect and self.frameProvider.imageEffect.frame is not None:
            resultFrame = self.frameProvider.imageEffect.getFrame().copy()
        else:
            resultFrame = frame.copy()

        if self.printTime:
            printTimeOnImage(resultFrame, self.frameProvider.frameDateTime)

        if self.frameProvider.motionDetectionBaseFrame and self.contours and nightFrame:
            resultContoursFrame = resultFrame.copy()
            self.contours.drawContours(resultContoursFrame, scale=self.frameProvider.motionDetectionBaseFrame.scaleFactor)
            self.resultFrame = resultContoursFrame
        else:
            self.resultFrame = resultFrame

        if self.recordFrames and self.recorder:
            self.recorder.addFrame(self.resultFrame)

    def setMotionWindow(self, value: float):
        self.motionWindow = timedelta(seconds=value)
        print('MotionWindow', self.motionWindow)


if __name__ == '__main__':
    from detectors.background_subtractor import BackgroundSubtractor
    from effects.motion_detection_base_frame import MotionDetectionBaseFrame
    from effects.trailing import Trailing
    from input.stream import Stream
    from tools.preview import Preview

    model = Model(
        Stream(
            url=0,
            # url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
            motionDetectionBaseFrame=MotionDetectionBaseFrame(
                scaleFactor=1,
                kernel=25),
            imageEffect=None),
        BackgroundSubtractor(30, 3, 0.01),
        None,
        Contours(0, ContoursTypes.Precise.name, 2),
        motionWindow=0)

    #model.recorder = Recorder(
    #    path='./ModelTest.mp4',
    #    resolution=model.frameProvider.resolution,
    #    fps=30,
    #    recordingSpeed=2,
    #    splitByDays=1)

    preview = Preview('Model')

    model.frameProvider.start()
    try:
        while True:
            model.processFrame()
            if model.resultFrame is not None and model.frameProvider:
                if model.recorder:
                    model.recorder.addFrame(model.resultFrame)

                preview.showFrame(
                    model.resultFrame,
                    (model.resultFrame.shape[1], model.resultFrame.shape[0]),
                    '{} {} {}'.format(
                        int(model.frameProvider.fpsCounter.averageFps),
                        int(model.fpsCounter.averageFps),
                        model.fpsCounter.frameId
                    )
                )
    except KeyboardInterrupt:
        pass

    if model.frameProvider:
        model.frameProvider.stop()
