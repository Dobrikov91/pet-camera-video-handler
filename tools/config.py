from enum import Enum
from pathlib import Path

from detectors.contours import ContoursTypes
from detectors.motion_detector import MotionDetectorTypes
from effects.image_effect import ImageEffectTypes

class MotionRecordingMode(Enum):
    Motion = 0
    Static = 1
    All = 2

defaultConfig = {
    'controls': {
        'streamUrl': 'http://',
        'blurKernel': 15,
        'inputFolder': str(Path.home()),
        'scaleFactor': 1,
        'printTime': True,
        'motionWindow': 2,
        'speedUp': 1,
    },
    'recording': {
        'fps': 30,
        'recordingSpeed': 1,
        'splitByDays': False,
        'onlyNightFrames': False,
        'recordingMode': MotionRecordingMode.Motion.name,
        'folderToSave': str(Path.home()),
        'filePrefix': 'PetCam'
    },
    'backSub': {
        'threshold': 50,
        'learningRate': 0.5,
        'dilationKernel': 3,
    },
    'custom': {
        'threshold': 50,
        'baseFrameRefresh': 1,
        'dilationKernel': 1,
    },
    'contours': {
        'thresholdSize': 10,
        'type': ContoursTypes.Precise.name,
        'width': 1,
    },
    'effects': {
        'effects': ImageEffectTypes.Empty.name,
        'strength': 0.8,
        'length': 30,
        'type': 0,
    },
    'detector': {
        'detector': MotionDetectorTypes.BackgroundSubtractor.name
    }
}
