#import os
from datetime import datetime

import cv2 as cv
#from PIL import Image, ImageDraw, ImageFont, UnidentifiedImageError

from tools.utils import creationTimeFromName, getStrDateTime

'''
def testFont():
    path = 'F0007544.JPG'
    newPath = 'font_test.jpg'
    printCreationTimeOnImage(path, newPath)

def printCreationTimeOnImage(path, newPath=None):
    if newPath == None:
        newPath = path

    try:
        img = Image.open(path)
        draw = ImageDraw.Draw(img)

        # ls -R /System/Library/Fonts | grep ttf
        # font = ImageFont.truetype(<font-file>, <font-size>)
        multiply = 1
        font = ImageFont.truetype("SFNSMono.ttf", 36 * multiply)

        # draw.text((x, y),"Sample Text",(r,g,b))
        position = (img.width - 430 * multiply, img.height - 50 * multiply)
        draw.text(position, creationTimeFromName(path), (0, 255, 0), font=font)
        #draw.text(position, creationTimeFromMeta(path), (0, 255, 0), font=font)
        # print('Converted {}'.format(path))
        img.save(newPath)
    except UnidentifiedImageError as e:
        print(e)
        os.remove(path)
'''

def printTimeOnImage(image: cv.Mat, time: datetime):
    scale = image.shape[0] / 720
    position = (int(image.shape[1] - 380 * scale), int(image.shape[0] - 20 * scale))
    font = cv.FONT_HERSHEY_SIMPLEX
    return cv.putText(image, getStrDateTime(time), position, font, scale, (0, 255, 0), int(2 * scale), cv.LINE_AA)

if __name__ == '__main__':
    from input.stream import Stream
    from tools.preview import Preview

    stream = Stream(
        # url=0,
        url='./testVideos/bbb_sunflower_2160p_30fps_normal.mp4',
        motionDetectionBaseFrame=None,
        imageEffect=None)
    preview = Preview()

    try:
        while True:
            if stream.frame is not None:
                frame = printTimeOnImage(stream.frame, datetime.now())
                preview.showFrame(
                    frame,
                    stream.resolution,
                    "{}".format(stream.resolution))
    except KeyboardInterrupt:
        pass
    stream.stop()