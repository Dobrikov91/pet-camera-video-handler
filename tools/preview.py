from typing import Any
import cv2 as cv

class Preview():
    def __init__(self, name='Preview'):
        self.name = name

    def __del__(self):
        cv.destroyAllWindows()

    def showFrame(self, frame: cv.Mat, resolution: tuple, text: Any):
        resizeFactor = 1280 / resolution[0]
        resizedFrame = cv.resize(frame, (1280, int(resolution[1] * resizeFactor)))
        resizedFrame = cv.putText(resizedFrame, str(text), (50 ,50), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv.LINE_AA)
        cv.imshow(self.name, resizedFrame)
        cv.waitKey(1)