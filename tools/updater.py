import requests
from kivy.utils import platform

# names from gitlab
def get_os_name():
    if platform == 'linux':
        return 'Ubuntu x64'
    elif platform == 'win':
        return 'Windows x64'
    elif platform == 'macosx':
        return 'MacOS Arm'
    else:
        return ''

# get latest release gitlab
def get_latest_release_gitlab() -> tuple:
    url = 'https://gitlab.com/api/v4/projects/Dobrikov91%2Fpet-camera-video-handler/releases'
    response = requests.get(url, timeout=5)
    if response.status_code == 200:
        # print json formatted string
        # import json
        # print(json.dumps(response.json()[0]['assets']['links'], indent=4, sort_keys=True))

        for link in response.json()[0]['assets']['links']:
            if link['name'] == get_os_name():
                return response.json()[0]['tag_name'][1:], link['url'] # remove 'v' from tag name
    return '0.0.0', ''

if __name__ == '__main__':
    print(get_latest_release_gitlab())