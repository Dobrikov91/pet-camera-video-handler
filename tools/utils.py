import glob
import os
import subprocess
import sys
from datetime import datetime

import cv2 as cv
import numpy as np


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath('.')

    return os.path.join(base_path, relative_path)

def creationTimeFromMeta(path: str) -> str:
    fileStats = os.stat(path)
    creationTime = datetime.fromtimestamp(fileStats.st_birthtime)
    # MSK TZ +3
    # creation_date -= datetime.timedelta(hours=3)
    return str(creationTime)

# Pattern f2021_11_30_02_09_40.jpg
def creationTimeFromName(path: str) -> str:
    filename = os.path.basename(path)
    (year, month, day, hour, minute, second) = filename.split('_')
    year = year[1:]
    second, *_ = second.split('.')

    creationTime = datetime(
        year=int(year),
        month=int(month),
        day=int(day),
        hour=int(hour),
        minute=int(minute),
        second=int(second))
    return str(creationTime)

def encodeVideo(path: str, videoName: str, frameRate: int) -> None:
    # -pattern_type glob -i '*.jpg'
    subprocess.run(
        ['ffmpeg',
        '-framerate', '{}'.format(frameRate),
        '-pattern_type', 'glob',
        '-i', '{}*.jpg'.format(path),
        '-c:v', 'libx264',
        #'-c:v', 'libx265',
        #'-crf', '0',
        '-preset', 'ultrafast',
        '-filter:v', 'fps={}'.format(min(30, frameRate)),
        '{}../{}.mp4'.format(path, videoName)])

def cleanFolder(path):
    filesToRemove = glob.glob('{}*'.format(path))
    for f in filesToRemove:
        os.remove(f)

def getStrDateTime(time):
    strTime = str(time)
    if '.' not in strTime:
        return strTime
    return strTime[:strTime.find('.')]

def cv2frameFromBinary(data):
    nparr = np.frombuffer(data, np.uint8)
    return cv.imdecode(nparr, cv.IMREAD_UNCHANGED)

def consoleLog(str: str = '', newline: bool = True):
    print('', end='\r')
    print(str, end='')
    if newline:
        print('', end='\n')
