import cv2 as cv
import numpy as np
from input.frame_provider import FrameProvider
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.camera import Camera

Builder.load_string('''
<ACamera>:
    resolution: (640, 480)
    play: False
''')

class ACamera(Camera):
    pass

class AndroidSource():
    def __init__(self, camera: Camera) -> None:
        self.camera = camera
        print(self.camera)
        self.camera.play = True
        self.frame = None
        print('STARTED')

    def frameFromTexture(self, dt):
        try:
            texture = self.camera.texture
            pixels = texture.pixels
            nparr = np.frombuffer(pixels, np.uint8).reshape(480, 640, 4)
            self.frame = cv.cvtColor(nparr, cv.COLOR_RGB2BGR)
        except Exception as e:
            print(e)

    def read(self):
        from time import sleep
        Clock.schedule_once(self.frameFromTexture)
        sleep(1 / 30)
        if self.frame is None:
            return False, None
        return True, self.frame

    def release(self):
        self.camera.play = False
        pass

    def isOpened(self) -> bool:
        return True
