from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout

from items.sliders import ConfigSliderFloat, ConfigSliderInt

Builder.load_string('''
<BackSubView>:
    name: 'backSub'
    orientation: 'vertical'
    size_hint_y: None
    height: self.minimum_height

    ConfigSliderInt:
        id: threshold
        section: root.name
        name: 'threshold'
        min: 1
        max: 301
        step: 1

    ConfigSliderFloat:
        id: learningRate
        section: root.name
        name: 'learningRate'
        min:   0.0
        max:   0.5
        step:  0.01
        suffix: '{:.2f} s'.format(1.0 / self.value) if self.value > 0 else 'inf'

    ConfigSliderInt:
        id: dilationKernel
        section: root.name
        name: 'dilationKernel'
        min: 0
        max: 11
        step: 1

    ConfigSliderInt:
        id: thresholdSize
        section: 'contours'
        name: 'thresholdSize'
        min: 0
        max: 100
        step: 1
''')

class BackSubView(MDBoxLayout):
    pass