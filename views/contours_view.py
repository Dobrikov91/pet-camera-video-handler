from kivy.app import App
from kivy.lang import Builder
from kivy.metrics import dp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRectangleFlatButton, MDRaisedButton
from kivymd.uix.menu import MDDropdownMenu
from kivy.clock import Clock

from detectors.contours import ContoursTypes
from items.sliders import ConfigSliderInt

Builder.load_string('''
<ContoursWidget>:
    name: 'contours'
    orientation: 'vertical'
    size_hint_y: None
    height: self.minimum_height

    ConfigSliderInt:
        id: width
        section: root.name
        name: 'width'
        min: -1
        max: 10
        step: 1

<ContoursView>:
    id: contoursView
    orientation: 'vertical'
    size_hint_y: None
    height: self.minimum_height
    md_bg_color: app.theme_cls.bg_light
    spacing: dp(4)

    BoxLayout:
        orientation: 'horizontal'
        size_hint_y: None
        height: dp(36)

        MDLabel:
            text: 'Contours:'

        ContoursTypeSelector:
            size_hint_x: 0.5
            root: root
            on_release: self.menu.open()
            md_bg_color: (0.5, 0.5, 0.5, 1) if self.text == 'Empty' else app.theme_cls.primary_color
''')

class ContoursWidget(MDBoxLayout):
    pass

class ContoursTypeMenu(MDDropdownMenu):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.items = [
            {
                'viewclass': 'OneLineListItem',
                'height': dp(36),
                'text': type.name,
                'on_release': lambda x=type.name: self.setContoursType(x),
            } for type in ContoursTypes]

        self.position='top'
        self.width_mult=3

    def setContoursType(self, name):
        App.get_running_app().config.set('contours', 'type', name)
        self.caller.text = name
        self.caller.root.setWidget(0)
        self.dismiss()

class ContoursTypeSelector(MDRaisedButton):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.text = App.get_running_app().config.get('contours', 'type')
        self.menu = ContoursTypeMenu()
        self.menu.caller = self

class ContoursView(MDBoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.subwidget = None
        Clock.schedule_once(self.setWidget, 0)

    def setWidget(self, dt):
        if self.subwidget:
            self.remove_widget(self.subwidget)

        curContoursType = App.get_running_app().config.get('contours', 'type')

        if curContoursType != ContoursTypes.Empty.name:
            self.subwidget = ContoursWidget()
        else:
            self.subwidget = None

        if self.subwidget:
            self.add_widget(self.subwidget)