from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout

from items.buttons import ConfigToggleButton
from items.frame_selector import FrameSelector
from items.sliders import ConfigSliderInt

Builder.load_string('''
<ControlsView>:
    name: 'controls'
    orientation: 'vertical'
    md_bg_color: app.theme_cls.bg_light
    size_hint_y: None
    height: self.minimum_height
    spacing: dp(4)

    ConfigSliderFPS:
        id: speedUp
        section: root.name
        name: 'speedUp'

    ConfigSliderScale:
        id: scaleFactor
        section: root.name
        name: 'scaleFactor'

    ConfigSliderInt:
        id: blurKernel
        section: root.name
        name: 'blurKernel'
        suffix: 'px'
        min: 1
        max: 101
        step: 2

    ConfigToggleButton:
        id: printTime
        section: root.name
        name: 'printTime'
        text: 'Print time'
        size_hint_x: 1
''')

class ControlsView(MDBoxLayout):
    pass
