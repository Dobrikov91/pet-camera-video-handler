from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout

from items.sliders import ConfigSliderInt, ConfigSliderTime

Builder.load_string('''
<CustomDetectorView>:
    name: 'custom'
    orientation: 'vertical'
    size_hint_y: None
    height: self.minimum_height

    ConfigSliderInt:
        id: threshold
        section: root.name
        name: 'threshold'
        min: 1
        max: 101
        step: 1

    ConfigSliderTime:
        id: baseFrameRefresh
        section: root.name
        name: 'baseFrameRefresh'
        values: [0, 1, 2, 3, 5, 10, 15, 20, 30, 45, 1 * 60, 2 * 60, 3 * 60, 5 * 60, 10 * 60, 15 * 60, 20 * 60, 30 * 60, 45 * 60, 60 * 60]

    ConfigSliderInt:
        id: dilationKernel
        section: root.name
        name: 'dilationKernel'
        min: 0
        max: 11
        step: 1

    ConfigSliderInt:
        id: thresholdSize
        section: 'contours'
        name: 'thresholdSize'
        min: 0
        max: 100
        step: 1
''')

class CustomDetectorView(MDBoxLayout):
    pass
