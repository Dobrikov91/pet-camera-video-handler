from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.metrics import dp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRectangleFlatButton, MDRaisedButton
from kivymd.uix.gridlayout import MDGridLayout
from kivymd.uix.menu import MDDropdownMenu

from effects.image_effect import ImageEffectTypes
from effects.trailing import TrailingType
from items.sliders import ConfigSliderFloat, ConfigSliderTime

Builder.load_string('''
<BlurView>
    section: 'effects'
    name: 'strength'
    suffix: '%'
    min: 0.6
    max: 1
    step: 0.01
    customValue: int(self.value * 100)
    valueText: '{} {}'.format(int(self.customValue), self.suffix)

<TrailingView>
    orientation: 'horizontal'
    size_hint_y: None
    height: self.minimum_height

    ConfigSliderTime:
        section: 'effects'
        name: 'length'
        values: [1, 2, 3, 5, 10, 15, 20, 30, 45, 1 * 60, 2 * 60, 3 * 60, 5 * 60, 10 * 60, 15 * 60, 20 * 60, 30 * 60, 45 * 60, 1 * 3600, 2 * 3600, 3 * 3600, 4 * 3600, 6 * 3600, 8 * 3600, 12 * 3600, 24 * 3600, 48 * 3600, 96 * 3600]

    TrailingTypeSelector:
        size_hint_x: 0.5
        on_press: self.menu.open()

<EffectsView>
    md_bg_color: app.theme_cls.bg_light
    orientation: 'vertical'
    height: self.minimum_height
    size_hint_y: None
    spacing: dp(4)

    MDBoxLayout:
        orientation: 'horizontal'
        size_hint_y: None
        height: dp(36)

        MDLabel:
            text: 'Effect:'

        EffectsTypeSelector:
            root: root
            size_hint_x: 0.5
            on_press: self.menu.open()
            md_bg_color: (0.5, 0.5, 0.5, 1) if self.text == 'Empty' else app.theme_cls.primary_color
''')

class BlurView(ConfigSliderFloat):
    pass

class TrailingView(MDBoxLayout):
    pass

class EffectsTypeMenu(MDDropdownMenu):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.items = [
            {
                'viewclass': 'OneLineListItem',
                'height': dp(36),
                'text': type.name,
                'on_release': lambda x=type.name: self.setEffectType(x),
            } for type in ImageEffectTypes]

        self.position='top'
        self.width_mult=3

    def setEffectType(self, name):
        App.get_running_app().config.set('effects', 'effects', name)
        self.caller.text = name
        self.caller.root.setWidget(0)
        self.dismiss()

class EffectsTypeSelector(MDRaisedButton):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.text = App.get_running_app().config.get('effects', 'effects')
        self.menu = EffectsTypeMenu()
        self.menu.caller = self

class TrailingTypeMenu(MDDropdownMenu):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.items = [
            {
                'viewclass': 'OneLineListItem',
                'height': dp(36),
                'text': TrailingType(type).name,
                'on_release': lambda x=type: self.setTrailingType(x),
            } for type in TrailingType]

        self.position='top'
        self.width_mult=3

    def setTrailingType(self, type: TrailingType):
        App.get_running_app().config.set('effects', 'type', type.value)
        self.caller.text = TrailingType(type).name
        self.dismiss()

class TrailingTypeSelector(MDRectangleFlatButton):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        value = App.get_running_app().config.get('effects', 'type')
        self.text = TrailingType(int(value)).name
        self.menu = TrailingTypeMenu()
        self.menu.caller = self

class EffectsView(MDBoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.name = 'effects'
        self.subwidget = None
        Clock.schedule_once(self.setWidget, 0)

    def setWidget(self, dt):
        if self.subwidget is not None:
            self.remove_widget(self.subwidget)

        curEffect = App.get_running_app().config.get('effects', 'effects')
        if curEffect == 'Blur':
            self.subwidget = BlurView()
        elif curEffect == 'Trailing':
            self.subwidget = TrailingView()
        else:
            self.subwidget = None

        if self.subwidget is not None:
            self.add_widget(self.subwidget)