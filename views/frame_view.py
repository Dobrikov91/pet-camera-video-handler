import cv2 as cv
import numpy as np
from kivy.graphics.texture import Texture
from kivy.lang import Builder
from kivy.uix.image import Image

Builder.load_string('''
<FrameView>:
    name: ''

    Label:
        center_x: self.parent.center_x
        center_y: self.parent.center_y
        text: '[color=FF0000]{}[/color]'.format(root.name)
        markup: True
''')

class FrameView(Image):
    def __init__(self, enabled: bool, name: str, **kwargs):
        super(FrameView, self).__init__(**kwargs)
        self.enabled = enabled
        self.name = name
        self.clear()

    def clear(self):
        self.load(np.full((720, 1280), 100, dtype=np.uint8))

    def load(self, frame):
        if frame is None:
            return

        if len(frame.shape) == 2: # grayscale
            frame = cv.cvtColor(frame, cv.COLOR_GRAY2RGB)
        else:
            frame = cv.cvtColor(frame, cv.COLOR_BGR2RGB)

        self.texture = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt='rgb') # android don't support bgr
        self.texture.blit_buffer(frame.tobytes(), colorfmt='rgb', bufferfmt='ubyte')
        self.texture.flip_vertical()