import cv2 as cv
from kivy.lang import Builder
from kivymd.uix.gridlayout import MDGridLayout

from views.frame_view import FrameView

Builder.load_string('''
<FramesView>:
    rows: 2
''')

class FramesView(MDGridLayout):
    frameNames = ['blured', 'output', 'diff', 'night']
    frames = {}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        for name in self.frameNames:
            self.frames[name] = FrameView(enabled=False, name=name)
        self.add('output', state=True)

    def add(self, name, state: bool):
        if self.frames[name].enabled == state:
            return

        self.frames[name].enabled = state
        self.add_widget(self.frames[name]) if state else self.remove_widget(self.frames[name])

    def show(self, name, frame):
        if self.frames[name].enabled:
            if name == 'output' or name == 'night':
                self.frames[name].load(frame)
            else:
                self.frames[name].load(cv.bitwise_not(frame))

    def clearAll(self):
        for name in self.frameNames:
            self.frames[name].clear()