import webbrowser
from os import path

from kivy.app import App
from kivy.lang import Builder
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.toolbar import MDTopAppBar

from items.buttons import SimpleToggleButton
from items.playback_controls import PlaybackControls
from tools.utils import resource_path
from views.contours_view import ContoursView
from views.controls_view import ControlsView
from views.effects_view import EffectsView
from views.motion_detectors_view import MotionDetectorsView
from views.overlapping_view import OverlappingView
from views.status_bar import StatusBar

Builder.load_string('''
<View>:
    orientation: 'vertical'
    buttonHeight: 36

    TopBar:
        id: topBar

    MDBoxLayout:
        orientation: 'horizontal'

        MDBoxLayout:
            orientation: 'vertical'
            padding: dp(8)
            spacing: dp(8)

            OverlappingView:
                id: overlappingView

            PlaybackControls:
                id: playbackControls

            MDBoxLayout:
                orientation: 'horizontal'
                size_hint_y: None
                height: self.minimum_height
                spacing: dp(8)

                ContoursView:
                    id: contoursView

                EffectsView:
                    id: effectsView

        MDBoxLayout:
            orientation: 'vertical'
            size_hint: None, None
            width: dp(8 * root.buttonHeight)
            height: self.minimum_height
            spacing: dp(8)
            padding: dp(8)

            ControlsView:
                id: controlsView

            MotionDetectorsView:
                id: motionDetectorsView

    StatusBar:
        id: statusBar
        size_hint_y: None
        height: '36dp'
        disabled: True
''')

class TopBar(MDTopAppBar):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.helpFile = 'PetCamera guide rus_1.0.2.pdf'
        self.helpFilePath = 'file://' + resource_path(self.helpFile)

        self.title = 'Select Source'
        self.left_action_items = [['menu']]
        self.right_action_items =[
            ['help', lambda _: webbrowser.open(self.helpFilePath)],
            ['information', lambda _: App.get_running_app().controller.showAbout()]]


class View(MDBoxLayout):
    pass
