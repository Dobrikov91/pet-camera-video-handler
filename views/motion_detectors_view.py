from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.metrics import dp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDRaisedButton
from kivymd.uix.menu import MDDropdownMenu

from detectors.motion_detector import MotionDetectorTypes
from views.back_sub_view import BackSubView
from views.custom_detector_view import CustomDetectorView

Builder.load_string('''
<MotionDetectorsView>:
    md_bg_color: app.theme_cls.bg_light
    orientation: 'vertical'
    size_hint_y: None
    height: self.minimum_height
    spacing: dp(4)

    MDLabel:
        size_hint_y: None
        height: dp(36)
        text: 'Motion detector type:'

    DetectorTypeSelector:
        root: root
        size_hint_x: 1
        on_release: self.menu.open()
        md_bg_color: (0.5, 0.5, 0.5, 1) if self.text == 'Empty' else app.theme_cls.primary_color
''')

class DetectorTypeMenu(MDDropdownMenu):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.items = [
            {
                'viewclass': 'OneLineListItem',
                'height': dp(36),
                'text': type.name,
                'on_release': lambda x=type.name: self.setDetectorType(x),
            } for type in MotionDetectorTypes]

        self.position='top'
        self.width_mult=3

    def setDetectorType(self, name):
        App.get_running_app().config.set('detector', 'detector', name)
        self.caller.text = name
        self.caller.root.setWidget(0)
        self.dismiss()

class DetectorTypeSelector(MDRaisedButton):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.text = App.get_running_app().config.get('detector', 'detector')
        self.menu = DetectorTypeMenu()
        self.menu.caller = self

class MotionDetectorsView(MDBoxLayout):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.subwidget = None
        Clock.schedule_once(self.setWidget)

    def setWidget(self, dt):
        if self.subwidget:
            self.remove_widget(self.subwidget)

        detectorType = App.get_running_app().config.get('detector', 'detector')

        if detectorType == 'BackgroundSubtractor':
            self.subwidget = BackSubView()
        elif detectorType == 'CustomDetector':
            self.subwidget = CustomDetectorView()
        else:
            self.subwidget = None

        if self.subwidget:
            self.add_widget(self.subwidget)