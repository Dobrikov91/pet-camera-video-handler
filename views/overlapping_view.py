from kivy.lang import Builder
from kivymd.uix.floatlayout import MDFloatLayout

from items.frame_selector import FrameSelector
from items.record_button import RecordButton
from items.source_selector import SourceSelector
from views.frames_view import FramesView

Builder.load_string('''
<OverlappingView>:
    md_bg_color: 0.5, 0.5, 0.5, 1

    FramesView:
        id: framesView
        pos_hint: {'center_x': .5, 'center_y': .5}

    SourceSelector:
        id: sourceSelector
        orientation: 'horizontal'
        pos_hint: {'top': 1, 'center_x': .5}
        md_bg_color: 0, 0, 0, 0.5
        size_hint: 1, None
        height: dp(36)

    MDBoxLayout:
        size_hint: 0.2, None
        pos_hint: {'y': 0, 'center_x': .5}
        md_bg_color: 0, 0, 0, 0.5
        height: dp(36)

        RecordButton:
            id: recordButton
            size_hint: 1, 1
            text: 'RECORD'
            disabled: True

    FrameSelector:
        spacing: dp(4)
        size_hint: None, None
        width: self.minimum_width
        height: self.minimum_height
        orientation: 'vertical'
        pos_hint: {'x': 0, 'center_y': .5}
        md_bg_color: 0, 0, 0, 0.5
''')

class OverlappingView(MDFloatLayout):
    pass