from kivy.event import EventDispatcher
from kivy.lang import Builder
from kivy.properties import NumericProperty, StringProperty
from kivymd.uix.gridlayout import MDGridLayout

Builder.load_string('''
<StatusBar>:
    rows: 2
    cols: 4

    Label:
        text: 'Input'
        size_hint_x: 0.5

    Label:
        text: 'Stream: {}x{:.0f}'.format(root.streamResolution, root.streamFps)

    Label:
        text: 'FPS: {:.2f}'.format(root.inputFps)

    Label:
        text: 'Frames: {}'.format(root.totalFrames)

    Label:
        text: 'Output'
        size_hint_x: 0.5

    Label:
        text: 'Contours: {}'.format(root.contours)

    Label:
        text: 'FPS: {:.2f}'.format(root.outputFps)

    Label:
        text: 'M-Frames: {}'.format(root.motionFrames)
''')

class StatusBar(MDGridLayout, EventDispatcher):
    streamResolution = StringProperty(str((0,0)))
    streamFps = NumericProperty(0)
    inputFps = NumericProperty(0)
    outputFps = NumericProperty(0)
    totalFrames = NumericProperty(0)
    motionFrames = NumericProperty(0)
    contours = NumericProperty(0)

    def update(self, streamResolution, streamFps, inputFps, outputFps, totalFrames, motionFrames, contours):
        self.streamResolution = str(streamResolution)
        self.streamFps = streamFps
        self.inputFps = inputFps
        self.outputFps = outputFps
        self.totalFrames = totalFrames
        self.motionFrames = motionFrames
        self.contours = contours

    def reset(self):
        self.update((0,0), 0, 0, 0, 0, 0, 0)
